# Code for Offshore CO<sub>2</sub> capture and utilization using floating wind/PV systems: site assessment and efficiency analysis in the Mediterranean 

This is the code used to generate the processed data and plots for the **energies** article [Offshore CO<sub>2</sub> capture and utilization using floating wind/PV systems: site assessment and efficiency analysis in the Mediterranean.]() It's loosely separated by the host machine it was run on.

Contact me, Doug Keller, if you have questions.
