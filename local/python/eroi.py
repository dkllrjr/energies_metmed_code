import system_analyze_seasonal as sr
import numpy as np

# ──────────────────────────────────────────────────────────────────────────

methanol_eg_dens = 4333.3 * 3600  # J/L

eff = []
for i in sr.methanols:
    eff.append(i['efficiency'])

meff = np.nanmean(eff)  # L/s / W

eroi = methanol_eg_dens * meff

print(eroi)

