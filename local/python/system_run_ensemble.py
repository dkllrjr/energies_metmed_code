# ──────────────────────────────────────────────────────────────────────────
# Script to run the simulated solar methanol island reactor for different 
# values of salinity and sea surface temperature
# ──────────────────────────────────────────────────────────────────────────

import pyseafuel
import xarray as xr
import numpy as np
import pandas as pd
from dask.diagnostics import ProgressBar
from scipy.integrate import simpson
import time
import multiprocessing as mp

import spherical_interpolation as sp

# ──────────────────────────────────────────────────────────────────────────

def save_xarray(data, lat, lon, path):
    data_xr = xr.DataArray(data, coords=dict(lon=(['y', 'x'], lon), lat=(['y', 'x'], lat)), dims=['y', 'x'])

    delayed = data_xr.to_netcdf(path, compute=False)

    with ProgressBar():
        results = delayed.compute()

# ──────────────────────────────────────────────────────────────────────────

host_data = '../../../host_data/seafuel/ensemble/'

sst = xr.open_dataarray(host_data + 'ensemble_average_SST_1D_2D.nc')
psu = xr.open_dataarray(host_data + 'ensemble_average_SAL_1D_2D.nc')
sst += 273.15

lat = sst.lat.values
lon = sst.lon.values

turbine = xr.open_dataarray(host_data + 'ensemble_average_TURB_interp.nc')
solar = xr.open_dataarray(host_data + 'ensemble_average_POW_2_interp.nc')

turbine = turbine.values
solar = solar.values

mwh = xr.open_dataarray('../data/Max_Wave_Heights.nc')

# ──────────────────────────────────────────────────────────────────────────
# seawater parameters

S = psu.values
SST = sst.values

# ──────────────────────────────────────────────────────────────────────────
# feed flows

arm_ratio = 0.001
seawater_in_degasser = 10  # L/s
seawater_in_desalinator = arm_ratio * seawater_in_degasser  # L/s

# ──────────────────────────────────────────────────────────────────────────
# desalinator parameters

water_ratio = .5
salt_removal = .99
stages = 1

# ──────────────────────────────────────────────────────────────────────────
# electrolyzer parameters

# for Pt/Ir
A_stack = 250  # cm^2
E0 = 1.4  # V
K = 27.8  # 1/ohm cm^2
R = 0.15  # ohm cm^2

# ──────────────────────────────────────────────────────────────────────────
# reactor parameters

P = 60  # bars;
T = 180  # C

# ══════════════════════════════════════════════════════════════════════════
# run reactor for points

methanol = sst.copy(deep=True)

degasser = {'type': 'bipolar_membrane', 'seawater_in': seawater_in_degasser}
desalinator = {'type': 'electrodialysis', 'seawater_in': seawater_in_desalinator, 'seawater_S': S, 'seawater_T': SST, 'water_ratio': water_ratio, 'salt_removal': salt_removal, 'stages': stages}
electrolyzer = {'type': 'Shen', 'E0': E0, 'K': K, 'R': R, 'area': A_stack}
reactor = {'type': 'plug_flow', 'P': P, 'T': T}

flows, power, misc = pyseafuel.system.seafuel(degasser, desalinator, electrolyzer, reactor)

m = flows['reactor']['ch3oh_out']  # L/s
p = power['total']  # W

m_e = m/p * 86400 * 1e3  # mL/day / W

methanol = m_e

print('saved co2')
print('ran methanol production')

# ──────────────────────────────────────────────────────────────────────────

td = np.arange(SST.shape[0])
ts = np.linspace(0, td.shape[0], solar.shape[0])

solar_meth = np.zeros(shape=(SST.shape[1], SST.shape[2]))
wind_meth = np.zeros(shape=(SST.shape[1], SST.shape[2]))
for i in range(SST.shape[1]):
    for j in range(SST.shape[2]):

        s_temp = solar[:,i,j]
        w_temp = turbine[:,i,j]
        m_temp = np.interp(ts, td, methanol[:,i,j])
        t = ts

        s_prod = m_temp * s_temp  # mL/day / m^2
        w_prod = m_temp * w_temp

        solar_meth[i,j] = simpson(s_prod, t)  # mL/m^2 in a year
        wind_meth[i,j] = simpson(w_prod, t)

# ──────────────────────────────────────────────────────────────────────────

save_path = host_data + 'solar_methanol_integrated_production.nc'
save_xarray(solar_meth, lat, lon, save_path)

save_path = host_data + 'turbine_methanol_integrated_production.nc'
save_xarray(wind_meth, lat, lon, save_path)

