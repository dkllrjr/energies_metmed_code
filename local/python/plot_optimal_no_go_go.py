# max sig. wave height = 1 = 0.5 max wave height
# 1 to 1.65 for tug boats towing
# 1.5 SWATH (Small Waterplane Area Twin Hull) crew transfer

# No go 1.5 >= x
# sorta go 1.5 > x >= 1
# go 1 > x

import xarray as xr
import numpy as np
import med_sea_plotting as msp

import spherical_interpolation as sp

# ──────────────────────────────────────────────────────────────────────────
# wave height

data = xr.open_dataarray('../data/Max_Wave_Heights.nc')

lat = data.lat.values
lon = data.lon.values

waves = data.values

title = 'Wave Height'
units = '$m$'

waves /= 2

wavesn = np.where(waves >= 1.5, 2, waves)
wavesn = np.where(1 > waves, 0, wavesn)
wavesn = np.where((1.5 > waves) & (waves >= 1), 1, wavesn)

# ──────────────────────────────────────────────────────────────────────────

host_data = '../../../host_data/seafuel/ensemble/'

solar = xr.open_dataarray(host_data + 'solar_methanol_integrated_production.nc')
wind = xr.open_dataarray(host_data + 'turbine_methanol_integrated_production.nc')

nemo_lat = solar.lat.values
nemo_lon = solar.lon.values

# 400 to 470 solar
# 470 up solar
# 140 to 350 wind

solar = solar.values
wind = wind.values

solarn = np.where(solar >= 470, 0, solar)
solarn = np.where((solar >= 400) & (470 > solar), 1, solarn)
solarn = np.where(400 > solar, 2, solarn)

windn = np.where(wind >= 350, 0, wind)
windn = np.where((wind >= 140) & (350 > wind), 1, windn)
windn = np.where(140 > wind, 2, windn)

# ──────────────────────────────────────────────────────────────────────────

wavesn = sp.sphere_interpolation(wavesn, lat, lon, nemo_lat, nemo_lon)
wavesn = np.where(np.isnan(solarn), np.nan, wavesn)

# ──────────────────────────────────────────────────────────────────────────

windn_overall = np.where((wavesn == 2) | (windn == 2), 2, windn)
windn_overall = np.where(((wavesn == 1) | (windn == 1)) & (~(wavesn == 2) | ~(windn == 2)), 1, windn_overall)

solarn_overall = np.where((wavesn == 2) | (solarn == 2), 2, solarn)
solarn_overall = np.where(((wavesn == 1) | (solarn == 1)) & (~(wavesn == 2) | ~(solarn == 2)), 1, solarn_overall)

data = [wavesn, solarn_overall, windn_overall]
plot_path = '../plots/optimal_go_no_go.png'

titles = ['Maximum Significant Wave Height', 'Solar Powered Islands', 'Wind Powered Islands']

title_size = 24
font_size = 20
fsize = font_size

fig, ax = msp.fig_med_plot(3, 1, 18, 16)
ax = ax.flatten()
fig.suptitle('Optimal Locations', y=.99, fontsize=title_size + 10)

for i, axi in enumerate(ax):
    axi, med_map = msp.ax_med_plot_block(axi, data[i], nemo_lat, nemo_lon, titles[i], units, font_size, title_size)

    # setting up the plot colorbar
    fig.subplots_adjust(right=0.85)
    cbar_ax = fig.add_axes([0.87, 0.02, 0.02, 0.94])
    # cbar = fig.colorbar(med_map, cax=cbar_ax, orientation='vertical', pad=0.005, ticks=[1/3, 1, 2-1/3])
    cbar = fig.colorbar(med_map, cax=cbar_ax, orientation='vertical', pad=0.005, ticks=[2-1/3, 1, 1/3])
    cbar.ax.set_yticklabels(['Poor', 'Marginal', 'Good'])  # vertically oriented colorbar
    # cbar.set_label(units, fontsize=font_size, labelpad=30)
    cbar.ax.tick_params(labelsize=font_size, pad=5)
    cbar.ax.invert_yaxis()


# adjusting the plot to better fit the saved image area
# fig.subplots_adjust(left=0.1, right=0.94, bottom=0.04, top=0.92, hspace=0.3)
fig.subplots_adjust(left=0.1, bottom=0.04, top=0.92, hspace=0.3)

fig.text(0.05, 0.93, '(a)', fontsize=fsize-2, fontweight='bold')
fig.text(0.05, 0.625, '(b)', fontsize=fsize-2, fontweight='bold')
fig.text(0.05, 0.3, '(c)', fontsize=fsize-2, fontweight='bold')

msp.save_med_plot(fig, plot_path)
