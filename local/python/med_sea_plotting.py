import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from mpl_toolkits.axes_grid1 import make_axes_locatable

from matplotlib.colors import ListedColormap

from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import cartopy.crs as ccrs
import cartopy.feature as cfeature

import numpy as np

import xarray as xr

# ──────────────────────────────────────────────────────────────────────────

def med_plot(var_grid, lat_grid, long_grid, var_min, var_max, title, units, plot_path, levels=20, cmap='gist_rainbow_r'):
    """Plots a 2D map of a scalar variable in the Mediterranean Sea.

    Parameters
    ----------
    var_grid : 2D array_like
        2D array of a scalar variable

    lat_grid : 2D array_like
        2D array of the latitude points corresponding to the locations of `var_grid`.

    long_grid : 2D array_like
        2D array of the longitude points corresponding to the locations of `var_grid`.

    var_min : float
        Scalar for desired minimum of `var_grid` to be sectioned and plotted.

    var_max : float
        Scalar for desired maximum of `var_grid` to be sectioned and plotted.

    title : str
        Plot title

    units : str
        Units of the data

    plot_path : string
        Path to save the plot to.

    Notes
    -----
    This a sample program to plot a scalar in 2 dimensions.
    """

    # font sizes for the plot
    font_size = 28
    title_size = 34
    
    # initializing the plot; plt.subplots is just the easiest way I've found to make a plot with the flexibility I want
    fig, ax = plt.subplots(1, 1, figsize=(18, 7), dpi=400, subplot_kw={'projection': ccrs.PlateCarree(5)})

    # download coastline data at 10m resolution
    coast_10m = cfeature.NaturalEarthFeature('physical', 'land', '10m', edgecolor='black', facecolor='white') 
    
    # setting up the map plot
    gl = ax.gridlines(crs=ccrs.PlateCarree(), draw_labels=True, linewidth=.75, color='black', alpha=0.35, linestyle='--', zorder=6)  # setting up gridlines for lat. long.
    gl.xlabels_top = False  # removing upper and right lat. long. labels
    gl.ylabels_right = False
    gl.xlocator = mticker.FixedLocator([-90, -10, 0, 10, 20, 30, 90])  # sets the gridline locations; longitude
    gl.ylocator = mticker.FixedLocator([0, 30, 35, 40, 45, 90])  # latitude
    gl.xformatter = LONGITUDE_FORMATTER  # formatter needed to get proper labels
    gl.yformatter = LATITUDE_FORMATTER
    gl.xlabel_style = {'size': font_size}  # setting label font size
    gl.ylabel_style = {'size': font_size}
    gl.xpadding = 20  # padding the lat. long. labels; sometimes they get squished into the plot
    gl.ypadding = 20
    
    ax.add_feature(coast_10m, zorder=5)  # adding coastline to plot
    ax.set_extent([-7, 37.25, 29.75, 46.25], crs=ccrs.PlateCarree())  # setting plot area
    
    levels = np.round(np.linspace(var_min, var_max, levels), 2)  # setting the levels of the filled contours
    med_map = ax.contourf(long_grid, lat_grid, var_grid, transform=ccrs.PlateCarree(), vmin=var_min, vmax=var_max, levels=levels, extend='both', cmap=cmap)  # plotting filled contours of the data

    # setting up the plot colorbar
    cbar = plt.colorbar(med_map, orientation='vertical', pad=0.015)
    cbar.set_label(units, fontsize=font_size - 4)
    cbar.ax.tick_params(labelsize=font_size - 4)

    med_map_contours = ax.contour(long_grid, lat_grid, var_grid, transform=ccrs.PlateCarree(), vmin=var_min, vmax=var_max, levels=levels, extend='both', colors='black', alpha=.5, linewidths=1)  # plotting contour lines of the same data; doesn't necessarily have to be the same data
    
    # inline labels for contour lines
    ax.clabel(med_map_contours, med_map_contours.levels, inline=False, fontsize=4)
    
    # setting title
    ax.set_title(title, pad=20, fontsize=title_size)

    # adjusting the plot to better fit the saved image area
    fig.subplots_adjust(top=0.90, left=0.08, right=1.01, bottom=0.1)

    # saving and closing the plot
    fig.savefig(plot_path)
    plt.close()

# ──────────────────────────────────────────────────────────────────────────

def med_plot_seasonal(data, lat, long, var_min, var_max, titles, units, suptitle, font_size, title_size, plot_path, cmap='gist_rainbow_r'):

    fig, ax = fig_med_plot(2, 2, 12, 30)
    ax = ax.flatten()
    fig.suptitle(suptitle, y=.96, fontsize=title_size + 10)

    for i, axi in enumerate(ax):
        axi, med_map = ax_med_plot(axi, data[i], lat, long, var_min, var_max, titles[i], units, font_size, title_size, cmap=cmap)

    # setting up the plot colorbar
    fig.subplots_adjust(right=0.8)
    cbar_ax = fig.add_axes([0.895, 0.15, 0.02, 0.7])
    cbar = fig.colorbar(med_map, cax=cbar_ax, orientation='vertical', pad=0.005)

    cbar.set_label(units, fontsize=font_size, labelpad=30)
    cbar.ax.tick_params(labelsize=font_size - 4, pad=10)

    # adjusting the plot to better fit the saved image area
    fig.subplots_adjust(left=0.06, bottom=0.07, right=0.87, wspace=0.15, hspace=0.3)

    save_med_plot(fig, plot_path)


def fig_med_plot(row, col, height, width):

    # initializing the plot; plt.subplots is just the easiest way I've found to make a plot with the flexibility I want
    fig, ax = plt.subplots(row, col, figsize=(width, height), dpi=400, subplot_kw={'projection': ccrs.PlateCarree(5)})

    return fig, ax


def ax_med_plot(ax, var_grid, lat_grid, long_grid, var_min, var_max, title, units, font_size, title_size, levels=20, cmap='gist_rainbow_r'):
    """Plots a 2D map of a scalar variable in the Mediterranean Sea.

    Parameters
    ----------
    ax :

    var_grid : 2D array_like
        2D array of a scalar variable

    lat_grid : 2D array_like
        2D array of the latitude points corresponding to the locations of `var_grid`.

    long_grid : 2D array_like
        2D array of the longitude points corresponding to the locations of `var_grid`.

    var_min : float
        Scalar for desired minimum of `var_grid` to be sectioned and plotted.

    var_max : float
        Scalar for desired maximum of `var_grid` to be sectioned and plotted.

    title : str
        Plot title

    units : str
        Units of the data

    plot_path : string
        Path to save the plot to.

    Notes
    -----
    This a sample program to plot a scalar in 2 dimensions.
    """

    # download coastline data at 10m resolution
    coast_10m = cfeature.NaturalEarthFeature('physical', 'land', '10m', edgecolor='black', facecolor='white') 
    
    # setting up the map plot
    gl = ax.gridlines(crs=ccrs.PlateCarree(), draw_labels=True, linewidth=.75, color='black', alpha=0.35, linestyle='--', zorder=6)  # setting up gridlines for lat. long.
    gl.xlabels_top = False  # removing upper and right lat. long. labels
    gl.ylabels_right = False
    gl.xlocator = mticker.FixedLocator([-90, -10, 0, 10, 20, 30, 90])  # sets the gridline locations; longitude
    gl.ylocator = mticker.FixedLocator([0, 30, 35, 40, 45, 90])  # latitude
    gl.xformatter = LONGITUDE_FORMATTER  # formatter needed to get proper labels
    gl.yformatter = LATITUDE_FORMATTER
    gl.xlabel_style = {'size': font_size}  # setting label font size
    gl.ylabel_style = {'size': font_size}
    gl.xpadding = 20  # padding the lat. long. labels; sometimes they get squished into the plot
    gl.ypadding = 20
    
    ax.add_feature(coast_10m, zorder=5)  # adding coastline to plot
    ax.set_extent([-7, 37.25, 29.75, 46.25], crs=ccrs.PlateCarree())  # setting plot area
    
    levels = np.round(np.linspace(var_min, var_max, levels), 2)  # setting the levels of the filled contours
    med_map = ax.contourf(long_grid, lat_grid, var_grid, transform=ccrs.PlateCarree(), vmin=var_min, vmax=var_max, levels=levels, extend='both', cmap=cmap)  # plotting filled contours of the data

    med_map_contours = ax.contour(long_grid, lat_grid, var_grid, transform=ccrs.PlateCarree(), vmin=var_min, vmax=var_max, levels=levels, extend='both', colors='black', alpha=.5, linewidths=1)  # plotting contour lines of the same data; doesn't necessarily have to be the same data
    
    # inline labels for contour lines
    ax.clabel(med_map_contours, med_map_contours.levels, inline=False, fontsize=4)
    
    # setting title
    ax.set_title(title, pad=20, fontsize=title_size)

    return ax, med_map


def ax_med_plot_wind(ax, u, v, var_grid, lat_grid, long_grid, var_min, var_max, title, units, font_size, title_size, levels=20):
    """Plots a 2D map of a scalar variable in the Mediterranean Sea.

    Parameters
    ----------
    ax :

    var_grid : 2D array_like
        2D array of a scalar variable

    lat_grid : 2D array_like
        2D array of the latitude points corresponding to the locations of `var_grid`.

    long_grid : 2D array_like
        2D array of the longitude points corresponding to the locations of `var_grid`.

    var_min : float
        Scalar for desired minimum of `var_grid` to be sectioned and plotted.

    var_max : float
        Scalar for desired maximum of `var_grid` to be sectioned and plotted.

    title : str
        Plot title

    units : str
        Units of the data

    plot_path : string
        Path to save the plot to.

    Notes
    -----
    This a sample program to plot a scalar in 2 dimensions.
    """

    # download coastline data at 10m resolution
    coast_10m = cfeature.NaturalEarthFeature('physical', 'land', '10m', edgecolor='black', facecolor='white') 
    
    # setting up the map plot
    gl = ax.gridlines(crs=ccrs.PlateCarree(), draw_labels=True, linewidth=.75, color='black', alpha=0.35, linestyle='--', zorder=6)  # setting up gridlines for lat. long.
    gl.xlabels_top = False  # removing upper and right lat. long. labels
    gl.ylabels_right = False
    gl.xlocator = mticker.FixedLocator([-90, -10, 0, 10, 20, 30, 90])  # sets the gridline locations; longitude
    gl.ylocator = mticker.FixedLocator([0, 30, 35, 40, 45, 90])  # latitude
    gl.xformatter = LONGITUDE_FORMATTER  # formatter needed to get proper labels
    gl.yformatter = LATITUDE_FORMATTER
    gl.xlabel_style = {'size': font_size}  # setting label font size
    gl.ylabel_style = {'size': font_size}
    gl.xpadding = 20  # padding the lat. long. labels; sometimes they get squished into the plot
    gl.ypadding = 20
    
    ax.add_feature(coast_10m, zorder=5)  # adding coastline to plot
    ax.set_extent([-7, 37.25, 29.75, 46.25], crs=ccrs.PlateCarree())  # setting plot area
    
    levels = np.round(np.linspace(var_min, var_max, levels), 2)  # setting the levels of the filled contours
    med_map = ax.contourf(long_grid, lat_grid, var_grid, transform=ccrs.PlateCarree(), vmin=var_min, vmax=var_max, levels=levels, extend='max', cmap='jet')  # plotting filled contours of the data

    space = 4
    med_map_quivers = ax.quiver(long_grid[::space, ::space], lat_grid[::space, ::space], u[::space, ::space], v[::space, ::space], transform=ccrs.PlateCarree(), scale=200, headwidth=2, headlength=4.5, width=2e-3, pivot='mid')  # plotting wind speed quivers
    
    # setting title
    ax.set_title(title, pad=20, fontsize=title_size)

    return ax, med_map


def save_med_plot(fig, plot_path):

    # saving and closing the plot
    fig.savefig(plot_path)
    plt.close()


def med_plot_wind_seasonal(u, v, ws, lat, long, var_min, var_max, titles, units, suptitle, font_size, title_size, plot_path):

    fig, ax = fig_med_plot(2, 2, 12, 30)
    ax = ax.flatten()
    fig.suptitle(suptitle, y=.96, fontsize=title_size + 10)

    for i, axi in enumerate(ax):
        axi, med_map = ax_med_plot_wind(axi, u[i], v[i], ws[i], lat, long, var_min, var_max, titles[i], units, font_size, title_size)

    # setting up the plot colorbar
    fig.subplots_adjust(right=0.8)
    cbar_ax = fig.add_axes([0.895, 0.15, 0.02, 0.7])
    cbar = fig.colorbar(med_map, cax=cbar_ax, orientation='vertical', pad=0.005)

    cbar.set_label(units, fontsize=font_size, labelpad=30)
    cbar.ax.tick_params(labelsize=font_size - 4, pad=10)

    # winds
    winds = ['Mistral + Tramontane', 'Tramontana', 'Cierzo', 'Bora']
    loc = [(-1.5, 43.5), (3.75, 44.5), (-4.1, 41), (10.25, 44.5)]
    dloc = [(-3, 3.5), (-0.5, 4), (-3, 1.5), (5, 0)]
    for i, wind in enumerate(winds):
        locxy = (loc[i][0] + dloc[i][0], loc[i][1] + dloc[i][1])
        ax[0].annotate(wind, xy=loc[i], xytext=locxy, fontsize=font_size-4, transform=ccrs.PlateCarree(), zorder=10, arrowprops=dict(facecolor='black', shrink=0.05), horizontalalignment='center', verticalalignment='center')

    winds = ['Mistral', 'Etesians', 'Levanter']
    loc = [(-1.5, 43.5), (23, 37), (-3, 36.75)]
    dloc = [(-3, 3.5), (4, 2), (0, -3)]
    for i, wind in enumerate(winds):
        locxy = (loc[i][0] + dloc[i][0], loc[i][1] + dloc[i][1])
        ax[2].annotate(wind, xy=loc[i], xytext=locxy, fontsize=font_size-4, transform=ccrs.PlateCarree(), zorder=10, arrowprops=dict(facecolor='black', shrink=0.05), horizontalalignment='center', verticalalignment='center')

    # adjusting the plot to better fit the saved image area
    fig.subplots_adjust(left=0.06, bottom=0.07, right=0.87, wspace=0.15, hspace=0.3)

    save_med_plot(fig, plot_path)

# ──────────────────────────────────────────────────────────────────────────

def med_plot_block(var_grid, lat_grid, long_grid, title, units, plot_path):
    """Plots a 2D map of a scalar variable in the Mediterranean Sea.

    Parameters
    ----------
    var_grid : 2D array_like
        2D array of a scalar variable

    lat_grid : 2D array_like
        2D array of the latitude points corresponding to the locations of `var_grid`.

    long_grid : 2D array_like
        2D array of the longitude points corresponding to the locations of `var_grid`.

    var_min : float
        Scalar for desired minimum of `var_grid` to be sectioned and plotted.

    var_max : float
        Scalar for desired maximum of `var_grid` to be sectioned and plotted.

    title : str
        Plot title

    units : str
        Units of the data

    plot_path : string
        Path to save the plot to.

    Notes
    -----
    This a sample program to plot a scalar in 2 dimensions.
    """

    # font sizes for the plot
    font_size = 28
    title_size = 34
    
    # initializing the plot; plt.subplots is just the easiest way I've found to make a plot with the flexibility I want
    fig, ax = plt.subplots(1, 1, figsize=(18, 7), dpi=400, subplot_kw={'projection': ccrs.PlateCarree(5)})

    # download coastline data at 10m resolution
    coast_10m = cfeature.NaturalEarthFeature('physical', 'land', '10m', edgecolor='black', facecolor='white') 
    
    # setting up the map plot
    gl = ax.gridlines(crs=ccrs.PlateCarree(), draw_labels=True, linewidth=.75, color='black', alpha=0.35, linestyle='--', zorder=6)  # setting up gridlines for lat. long.
    gl.xlabels_top = False  # removing upper and right lat. long. labels
    gl.ylabels_right = False
    gl.xlocator = mticker.FixedLocator([-90, -10, 0, 10, 20, 30, 90])  # sets the gridline locations; longitude
    gl.ylocator = mticker.FixedLocator([0, 30, 35, 40, 45, 90])  # latitude
    gl.xformatter = LONGITUDE_FORMATTER  # formatter needed to get proper labels
    gl.yformatter = LATITUDE_FORMATTER
    gl.xlabel_style = {'size': font_size}  # setting label font size
    gl.ylabel_style = {'size': font_size}
    gl.xpadding = 20  # padding the lat. long. labels; sometimes they get squished into the plot
    gl.ypadding = 20
    
    ax.add_feature(coast_10m, zorder=5)  # adding coastline to plot
    ax.set_extent([-7, 37.25, 29.75, 46.25], crs=ccrs.PlateCarree())  # setting plot area
    
    cmap = ListedColormap(['green', 'yellow', 'red'])
    med_map = ax.pcolor(long_grid, lat_grid, var_grid, transform=ccrs.PlateCarree(), vmin=0, vmax=2, cmap=cmap)  # plotting filled contours of the data

    # # setting up the plot colorbar
    # cbar = plt.colorbar(med_map, orientation='vertical', pad=0.015)
    # cbar.set_label(units, fontsize=font_size - 4)
    # cbar.ax.tick_params(labelsize=font_size - 4)

    # setting title
    ax.set_title(title, pad=20, fontsize=title_size)

    # adjusting the plot to better fit the saved image area
    fig.subplots_adjust(top=0.90, left=0.08, right=1.01, bottom=0.1)

    # saving and closing the plot
    fig.savefig(plot_path)
    plt.close()


def ax_med_plot_block(ax, var_grid, lat_grid, long_grid, title, units, font_size, title_size):
    """Plots a 2D map of a scalar variable in the Mediterranean Sea.

    Parameters
    ----------
    ax :

    var_grid : 2D array_like
        2D array of a scalar variable

    lat_grid : 2D array_like
        2D array of the latitude points corresponding to the locations of `var_grid`.

    long_grid : 2D array_like
        2D array of the longitude points corresponding to the locations of `var_grid`.

    title : str
        Plot title

    units : str
        Units of the data

    plot_path : string
        Path to save the plot to.

    Notes
    -----
    This a sample program to plot a scalar in 2 dimensions.
    """

    # download coastline data at 10m resolution
    coast_10m = cfeature.NaturalEarthFeature('physical', 'land', '10m', edgecolor='black', facecolor='white') 
    
    # setting up the map plot
    gl = ax.gridlines(crs=ccrs.PlateCarree(), draw_labels=True, linewidth=.75, color='black', alpha=0.35, linestyle='--', zorder=6)  # setting up gridlines for lat. long.
    gl.xlabels_top = False  # removing upper and right lat. long. labels
    gl.ylabels_right = False
    gl.xlocator = mticker.FixedLocator([-90, -10, 0, 10, 20, 30, 90])  # sets the gridline locations; longitude
    gl.ylocator = mticker.FixedLocator([0, 30, 35, 40, 45, 90])  # latitude
    gl.xformatter = LONGITUDE_FORMATTER  # formatter needed to get proper labels
    gl.yformatter = LATITUDE_FORMATTER
    gl.xlabel_style = {'size': font_size}  # setting label font size
    gl.ylabel_style = {'size': font_size}
    gl.xpadding = 20  # padding the lat. long. labels; sometimes they get squished into the plot
    gl.ypadding = 20
    
    ax.add_feature(coast_10m, zorder=5)  # adding coastline to plot
    ax.set_extent([-7, 37.25, 29.75, 46.25], crs=ccrs.PlateCarree())  # setting plot area
    
    cmap = ListedColormap(['tab:green', 'yellow', 'tab:red'])
    med_map = ax.pcolor(long_grid, lat_grid, var_grid, transform=ccrs.PlateCarree(), vmin=0, vmax=2, cmap=cmap, alpha=0.8)  # plotting filled contours of the data
    
    # setting title
    ax.set_title(title, pad=20, fontsize=title_size)

    return ax, med_map
