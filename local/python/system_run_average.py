# ──────────────────────────────────────────────────────────────────────────
# Script to run the simulated solar methanol island reactor for different 
# values of salinity and sea surface temperature
# ──────────────────────────────────────────────────────────────────────────

import pyseafuel
import xarray as xr
import numpy as np

# ──────────────────────────────────────────────────────────────────────────
## data

host_data = '../../../host_data/seafuel/average/'

psu = xr.open_dataarray(host_data + 'average_SAL_1D_2D.nc')  # salinity
sst = xr.open_dataarray(host_data + 'average_SST_1D_2D.nc')  # sea surface temperature
sst += 273.15

lat = psu.lat.values
lon = psu.lon.values

# ──────────────────────────────────────────────────────────────────────────
# seawater parameters

S = psu.values
SST = sst.values

# ──────────────────────────────────────────────────────────────────────────
# feed flows

seawater_in_degasser = 5  # L/s
seawater_in_desalinator = .005  # L/s

# ──────────────────────────────────────────────────────────────────────────
# desalinator parameters

water_ratio = .5
salt_removal = .99
stages = 1

# ──────────────────────────────────────────────────────────────────────────
# electrolyzer parameters

# for Pt/Ir
A_stack = 250  # cm^2
E0 = 1.4  # V
K = 27.8  # 1/ohm cm^2
R = 0.15  # ohm cm^2

# ──────────────────────────────────────────────────────────────────────────
# reactor parameters

P = 60  # bars;
T = 180  # C

# ──────────────────────────────────────────────────────────────────────────
# run reactor

degasser = {'type': 'bipolar_membrane', 'seawater_in': seawater_in_degasser}
desalinator = {'type': 'electrodialysis', 'seawater_in': seawater_in_desalinator, 'seawater_S': S, 'seawater_T': SST, 'water_ratio': water_ratio, 'salt_removal': salt_removal, 'stages': stages}
electrolyzer = {'type': 'Shen', 'E0': E0, 'K': K, 'R': R, 'area': A_stack}
reactor = {'type': 'plug_flow', 'P': P, 'T': T}

flows, power, misc = pyseafuel.system.seafuel(degasser, desalinator, electrolyzer, reactor)
