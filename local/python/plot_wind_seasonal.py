import xarray as xr
import med_sea_plotting as msp
import numpy as np
import matplotlib.pyplot as plt

# ──────────────────────────────────────────────────────────────────────────

def wind_speed(u, v):
    return (u**2 + v**2)**.5

# ──────────────────────────────────────────────────────────────────────────

host_data = '../../../host_data/seafuel/seasonal/'

# ──────────────────────────────────────────────────────────────────────────

uas = xr.open_dataarray(host_data + 'seasonal_average_UAS.nc')
vas = xr.open_dataarray(host_data + 'seasonal_average_VAS.nc')
betz = xr.open_dataarray(host_data + 'seasonal_average_BETZ.nc')
turb = xr.open_dataarray(host_data + 'seasonal_average_TURB.nc')

lat = uas.lat
long = uas.lon

# ──────────────────────────────────────────────────────────────────────────
# wind speed

ws = wind_speed(uas, vas)

var_min = 0
var_max = 7

titles = ['DJF', 'MAM', 'JJA', 'SON']
units = '$m/s$'
suptitle = 'Wind Speed'

wsdata = []
vdata = []
udata = []
for i, wsi in enumerate(ws):
    wsdata.append(wsi.values)
    udata.append(uas[i].values)
    vdata.append(vas[i].values)

# font sizes for the plot
font_size = 28
title_size = 34

plot_path = '../plots/wind_seasonal.png'

msp.med_plot_wind_seasonal(udata, vdata, wsdata, lat.values, long.values, var_min, var_max, titles, units, suptitle, font_size, title_size, plot_path)

# # ──────────────────────────────────────────────────────────────────────────
# # turb

var_min = 0
# var_max = 300
var_max = 250

vdata = []
for i in turb:
    vdata.append(i.values)

turbdata = vdata

# suptitle = 'V90/2000 Power'
suptitle = 'SWT-3.6-120 Power'
units = '$W/m^2$'

plot_path = '../plots/turbine_seasonal.png'

msp.med_plot_seasonal(vdata, lat, long, var_min, var_max, titles, units, suptitle, font_size, title_size, plot_path, cmap='Spectral_r')

# # ──────────────────────────────────────────────────────────────────────────
# # betz

# var_min = 0
# var_max = 300

# vdata = []
# for i in betz:
    # vdata.append(i.values)

# suptitle = 'Betz Limit Power'
# units = '$W/m^2$'

# plot_path = '../plots/betz_seasonal.png'

# msp.med_plot_seasonal(vdata, lat, long, var_min, var_max, titles, units, suptitle, font_size, title_size, plot_path)

# # ──────────────────────────────────────────────────────────────────────────

# # for i in range(4):
    # # ws = sp.sphere_interpolation(wsdata[i], lat, long, sst_lat, sst_lon)
    # # tu = sp.sphere_interpolation(temp[i], lat, long, sst_lat, sst_lon)
    # # print('ws', i, np.nanmean(ws))

