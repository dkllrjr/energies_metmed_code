# methanol_flow = methanol['methanol_flow']
# methanol_efficiency = methanol['methanol_efficiency']
# degassed_co2_mass_flow = feeds['degassed_co2_mass_flow']
# total_flow_in = feeds['total_flow_in']
# total_power_consumed = power['total_power_consumed']

# # ──────────────────────────────────────────────────────────────────────────
# # daily production

# barrels_from_liters = 1/158.987  # bbl/L; liters to barrels
# rho_crude = 0.8  # kg/L; density of crude oil
# rho_ch3oh = 0.7863  # kg/L; liquid methanol density
# # tonnes are 1000 kg; kt = 10^6 kg

# daily_removed_co2 = degassed_co2_mass_flow * 86400 / 1000  # Mg/day or tonnes/day

# daily_methanol_production = methanol_flow * 86400  # L/day
# daily_barrels_methanol = daily_methanol_production * barrels_from_liters  # bbl/day
# daily_kilotonnes_methanol = daily_methanol_production * rho_ch3oh / 10**6  # kt/day

# daily_sea_water_feed = total_flow_in * 86400  # L/day

# daily_power_consumption = np.nanmean(total_power_consumed) * 86400 / 10**9  # GJ/day

# ──────────────────────────────────────────────────────────────────────────

    # seawater_tot = sea_water_carbon_dioxide_arm_flow + sea_water_hydrogen_arm_flow  # L/s

    # # flow efficiencies
    # methanol_production = methanol_flow/total_flow_in  # L/s / L/s
    # methanol_efficiency = methanol_flow/total_power_consumed  # L/s / W

    # R = sea_water_hydrogen_arm_flow / sea_water_carbon_dioxide_arm_flow  # volumetric raw feed input ratio
