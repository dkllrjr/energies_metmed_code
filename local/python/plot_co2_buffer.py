import pyseafuel
import numpy as np
import matplotlib.pyplot as plt

# ──────────────────────────────────────────────────────────────────────────

pH = np.linspace(0, 14, 100)
psu = 30
sst = 293.15
co2 = 370

dco2 = pyseafuel.dic.carbon_dioxide(co2, sst, psu, volume=False)  # mol/kg
hco3 = pyseafuel.dic.bicarbonate(sst, psu, dco2, pH)
co3 = pyseafuel.dic.carbonate(sst, psu, hco3, pH)

dic = dco2 + hco3 + co3

# ──────────────────────────────────────────────────────────────────────────

fsize = 18
tsize = 24

fig, ax = plt.subplots(1, 1, figsize=(8, 5), dpi=400)

ax.plot(pH, dco2/dic, color='black', label='CO$_2^*$')
ax.plot(pH, hco3/dic, color='tab:blue', label='HCO$_3^{-}$')
ax.plot(pH, co3/dic, color='tab:red', label='CO$_3^{2-}$')

ax.tick_params(axis='both', labelsize=fsize-2)

ax.set_ylabel('Fraction of DIC', fontsize=fsize)
ax.set_xlabel('pH', fontsize=fsize)
ax.set_xlim(pH[0], pH[-1])

ax.grid()

ax.legend(fontsize=fsize-4)

fig.tight_layout()

fig.savefig('../plots/carbonate_buffer_system.png')
