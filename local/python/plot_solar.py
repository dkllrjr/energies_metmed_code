import xarray as xr
import med_sea_plotting as msp

##############################################################################
# effective irradiance

data = xr.open_dataarray('../data/average_EFF_IRR.nc')

lat = data.lat.values
long = data.lon.values

var_min = 170
var_max = 260

title = 'Effective Irradiance'
units = '$W/m^2$'

plot_path = '../plots/solar_power_average.png'

msp.med_plot(data.values, lat, long, var_min, var_max, title, units, plot_path)

##############################################################################
# solar panel power

# data = xr.open_dataarray('../data/average_POW.nc')

# lat = data.lat.values
# long = data.lon.values

# var_min = data.min()
# var_max = data.max()

# title = 'Solar Panel Generation'
# units = '$W/m^2$'

# plot_path = '../plots/solar_panel_power.png'

# med_plot(data.values, lat, long, var_min, var_max, title, units, plot_path)
