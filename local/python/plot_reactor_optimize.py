##############################################################################
# Script to determine optimal h2 to co2 ratio
##############################################################################

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import pyseafuel
import pickle

##############################################################################

print('calculating P T')

N_co2_o = 1
N_h2_o = 3
N_co_o = 0

P = np.linspace(10, 60, 100)  # bars
T = np.linspace(180, 340, 100) + 273.15  # Kelvin

cf = np.zeros((T.size, P.size))

for i, Ti in enumerate(T):
    for j, Pj in enumerate(P):

        _, _, _, _, _, xi = pyseafuel.reactor.equilibrium(N_co_o, N_co2_o, N_h2_o, Pj, Ti)
        cf[i, j] = xi

cf = np.where(cf < 0, np.nan, cf)
cf = np.where(cf > 1, np.nan, cf)

Tx, Py = np.meshgrid(T, P, indexing='ij')

Tx -= 273.15

# ──────────────────────────────────────────────────────────────────────────
# Plot

tsize = 22
fsize = 18

fig, ax = plt.subplots(1, 1, figsize=(6, 6), dpi=400)

var = ax.contourf(Tx, Py, cf, levels=20, cmap='plasma')

divider = make_axes_locatable(ax)
cax = divider.append_axes('right', size='5%', pad=0.05)

colorbar = fig.colorbar(var, cax=cax, orientation='vertical')
colorbar.set_label(label='$\\xi$', size=fsize-2)
colorbar.ax.tick_params(labelsize=fsize-4)

ax.set_xlabel('T $^\\circ C$', fontsize=fsize)
ax.set_ylabel('P $bar$', fontsize=fsize)

# ax.set_title('$[CO_2^o] \\rightarrow [CH_3OH]$', fontsize=tsize, y=1.01)
ax.set_title('$\dot{N}^0_{CO_2} \\rightarrow \dot{N}_{CH_3OH}$', fontsize=tsize, y=1.01)

ax.tick_params(axis='both', labelsize=fsize-4)

fig.tight_layout()

fig.savefig('../plots/P_T_conversion_factor.png')
plt.close()

# ──────────────────────────────────────────────────────────────────────────
# combined

fig_c, ax_c = plt.subplots(1, 2, figsize=(11, 6), dpi=400)

var = ax_c[0].contourf(Tx, Py, cf, levels=20, cmap='copper')

divider = make_axes_locatable(ax_c[0])
cax = divider.append_axes('right', size='5%', pad=0.05)

colorbar = fig.colorbar(var, cax=cax, orientation='vertical')
colorbar.set_label(label='$\\xi$', size=fsize-2)
colorbar.ax.tick_params(labelsize=fsize-4)

ax_c[0].set_xlabel('T $^\\circ C$', fontsize=fsize)
ax_c[0].set_ylabel('P $bar$', fontsize=fsize)

ax_c[0].set_title('State Dependence', fontsize=tsize-2, y=1.01)

##############################################################################

print('calculating ratio')

# r = np.linspace(3, 1500, 10000)
r = np.linspace(3, 55, 100)

N_co_o = 0
N_co2_o = 1

P = np.linspace(10, 60, 100)
# P = np.linspace(10, 60, 3)
T = 180 + 273.15

cf_max = np.zeros(P.size)
cf_argmax = np.zeros(P.size)

cf_55 = np.zeros(P.size)

# cf_rec = np.zeros_like(cf_max)
# cf_argmin = np.zeros_like(cf_max)

for j, Pj in enumerate(P):

    cf = np.zeros((r.size))

    for i, ri in enumerate(r):

        N_h2_o = N_co2_o * ri

        _, _, _, _, _, xi = pyseafuel.reactor.equilibrium(N_co_o, N_co2_o, N_h2_o, Pj, T)

        cf[i] = xi

    # cf = np.where(cf < 0, np.nan, cf)
    cf = np.where(cf < .4, np.nan, cf)
    cf = np.where(cf > 1, np.nan, cf)
    
    # plt.plot(r, cf)
    # plt.show()

    cf_max[j] = np.nanmax(cf)
    cf_argmax[j] = r[np.nanargmax(cf)]

    # cf = np.where(cf < .8*cf_max[j], np.nan, cf)

    # cf_rec[j] = np.nanmin(cf)
    # cf_argmin[j] = r[np.nanargmin(cf)]

    cf_55[j] = cf[-1]

##############################################################################
# save data

data = {'cf_max': cf_max, 'r_max': cf_argmax, 'notes': 'the optimal area for h2 ratio to co2 is about 60; at about 100-200, the increased ratio brings extremely diminished returns; less than 60 leaves a lot of optimization on the table; the calculation is very unstable'}

with open('../data/optimal_h2_co2_ratio.pickle', 'wb') as file:
    pickle.dump(data, file)

##############################################################################
# plot

# fig, ax = plt.subplots(1, 2, figsize=(10, 6), dpi=400)

# ax[0].plot(P, cf_max, color='tab:blue')

# ax[0].set_xlabel('P $bar$', fontsize=fsize)
# ax[0].set_ylabel('$\\xi$', fontsize=fsize)

# ax[0].set_title('Pressure Dependence ($r \\rightarrow \\infty$)', fontsize=tsize-2)

# ax[0].set_xlim(P[0], P[-1])
# ax[0].set_ylim(cf_max[0], 1)

# ax[1].plot(r[np.invert(np.isnan(cf))], cf[np.invert(np.isnan(cf))], color='tab:blue')

# ax[1].set_xlim(r[0], 55)
# ax[1].set_ylim(cf[0], 1)

# ax[1].set_xlabel('$r = \\frac{[H^o_2]}{[CO^o_2]}$', fontsize=fsize)
# # ax[1].set_ylabel('Conversion Factor', fontsize=fsize)

# ax[1].set_title('Ratio Dependence (P = 60 $bar$)', fontsize=tsize-2)

# for axi in ax:
    # axi.grid()
    # axi.tick_params(axis='both', labelsize=fsize-4)

# # fig.suptitle('$[CO_2^o] \\rightarrow [CH_3OH]$', fontsize=tsize)
# fig.suptitle('$\dot{N}^0_{CO_2} \\rightarrow \dot{N}_{CH_3OH}$', fontsize=tsize)

# fig.tight_layout()

# fig.savefig('../plots/ratio_conversion_factor.png')
# plt.close()

# ──────────────────────────────────────────────────────────────────────────

fig, ax = plt.subplots(1, 1, figsize=(6, 6), dpi=400)

ax.plot(r[np.invert(np.isnan(cf))], cf[np.invert(np.isnan(cf))], color='tab:blue')

ax.set_xlim(r[0], 55)
ax.set_ylim(cf[0], 1)

# ax.set_xlabel('$r = \\frac{[H^o_2]}{[CO^o_2]}$', fontsize=fsize)
ax.set_xlabel('$r = \\frac{ \\dot{N}^0_{H_2} }{ \\dot{N}^0_{CO_2} }$', fontsize=fsize)

ax.set_title('Ratio Dependence (P = 60 $bar$)', fontsize=tsize-2)

ax.grid()
ax.tick_params(axis='both', labelsize=fsize-4)

fig.suptitle('$\dot{N}^0_{CO_2} \\rightarrow \dot{N}_{CH_3OH}$', fontsize=tsize)

fig.tight_layout()

fig.savefig('../plots/ratio_conversion_factor.png')
plt.close()

# ──────────────────────────────────────────────────────────────────────────

# Plot

ax_c[1].plot(r[np.invert(np.isnan(cf))], cf[np.invert(np.isnan(cf))], color='tab:blue')

ax_c[1].set_xlim(r[0], 55)
ax_c[1].set_ylim(cf[0], 1)

ax_c[1].set_xlabel('$r = \\frac{ \\dot{N}^0_{H_2} }{ \\dot{N}^0_{CO_2} }$', fontsize=fsize)

ax_c[1].set_title('Ratio Dependence (P = 60 $bar$)', fontsize=tsize-2, y=1.01)

ax_c[1].grid()

for axi in ax_c:
    axi.tick_params(axis='both', labelsize=fsize-4)

fig_c.suptitle('$\dot{N}^0_{CO_2} \\rightarrow \dot{N}_{CH_3OH}$', fontsize=tsize)

fig_c.text(0.05, 0.86, '(a)', fontsize=fsize-2, fontweight='bold')
fig_c.text(0.54, 0.87, '(b)', fontsize=fsize-2, fontweight='bold')

fig_c.tight_layout()
# fig_c.subplots_adjust()

fig_c.savefig('../plots/conversion_factor_combined.png')
plt.close()
