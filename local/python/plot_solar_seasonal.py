import xarray as xr
import med_sea_plotting as msp
import spherical_interpolation as sp
import numpy as np

# ──────────────────────────────────────────────────────────────────────────

host_data = '../../../host_data/seafuel/seasonal/'

sst = xr.open_dataarray(host_data + 'seasonal_average_SST_1D_2D.nc')

sst_lat = sst.lat.values
sst_lon = sst.lon.values

# ──────────────────────────────────────────────────────────────────────────
# shortwave downward

data = xr.open_dataarray(host_data + 'seasonal_average_RSDS.nc')

lat = data.lat.values
long = data.lon.values

var_min = 100
var_max = 330

vdata = []
for i in data:
    vdata.append(i.values)

titles = ['DJF', 'MAM', 'JJA', 'SON']
units = '$W/m^2$'
suptitle = 'Shortwave Radiation Downward'

# font sizes for the plot
font_size = 28
title_size = 34

plot_path = '../plots/shortwave_downward_seasonal.png'

msp.med_plot_seasonal(vdata, lat, long, var_min, var_max, titles, units, suptitle, font_size, title_size, plot_path)

# ──────────────────────────────────────────────────────────────────────────
# effective irradiance

data = xr.open_dataarray(host_data + 'seasonal_average_EFF_IRR_2.nc')

lat = data.lat.values
long = data.lon.values

var_min = 155
var_max = 287

vdata = []
for i in data:
    vdata.append(i.values)

temp = vdata

titles = ['DJF', 'MAM', 'JJA', 'SON']
units = '$W/m^2$'
suptitle = 'Effective Irradiance'

# font sizes for the plot
font_size = 28
title_size = 34

plot_path = '../plots/solar_power_seasonal.png'

cmap = 'Spectral_r'

msp.med_plot_seasonal(vdata, lat, long, var_min, var_max, titles, units, suptitle, font_size, title_size, plot_path, cmap=cmap)

# ──────────────────────────────────────────────────────────────────────────
# solar panel power

data = xr.open_dataarray(host_data + 'seasonal_average_POW_2.nc')

lat = data.lat.values
long = data.lon.values

var_min = 75
var_max = 123

vdata = []
for i in data:
    vdata.append(i.values)

titles = ['DJF', 'MAM', 'JJA', 'SON']
units = '$W/m^2$'
suptitle = 'Solar Panel Generation'

# font sizes for the plot
font_size = 28
title_size = 34

plot_path = '../plots/solar_panel_power_seasonal.png'

msp.med_plot_seasonal(vdata, lat, long, var_min, var_max, titles, units, suptitle, font_size, title_size, plot_path, cmap=cmap)

# ──────────────────────────────────────────────────────────────────────────

for i in range(4):
    ef = sp.sphere_interpolation(temp[i], lat, long, sst_lat, sst_lon)
    print('eff irr', i, np.nanmean(ef))

    solp = sp.sphere_interpolation(vdata[i], lat, long, sst_lat, sst_lon)
    print('sol panel', i, np.nanmean(solp))

