import xarray as xr
import med_sea_plotting as msp

# ──────────────────────────────────────────────────────────────────────────

host_data = '../../../host_data/seafuel/seasonal/'

# ──────────────────────────────────────────────────────────────────────────

sst = xr.open_dataarray(host_data + 'seasonal_average_SST_1D_2D.nc')
psu = xr.open_dataarray(host_data + 'seasonal_average_SAL_1D_2D.nc')

lat = sst.lat
long = sst.lon

# ──────────────────────────────────────────────────────────────────────────
# sst

var_min = 11
var_max = 27

titles = ['DJF', 'MAM', 'JJA', 'SON']
units = '$^\\circ C$'
suptitle = 'Sea Surface Temperature'

vdata = []
for i in sst:
    vdata.append(i.values)

# font sizes for the plot
font_size = 28
title_size = 34

plot_path = '../plots/sst_seasonal.png'

msp.med_plot_seasonal(vdata, lat, long, var_min, var_max, titles, units, suptitle, font_size, title_size, plot_path, cmap='coolwarm')

# ──────────────────────────────────────────────────────────────────────────
# psu

var_min = 36.25
var_max = 40

vdata = []
for i in psu:
    vdata.append(i.values)

suptitle = 'Sea Surface Salinity'
units = '$PSU$'

plot_path = '../plots/salinity_seasonal.png'

msp.med_plot_seasonal(vdata, lat, long, var_min, var_max, titles, units, suptitle, font_size, title_size, plot_path, cmap='cool')
