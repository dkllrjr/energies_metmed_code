import pandas
import matplotlib.pyplot as plt
import numpy as np

##############################################################################
# load data

df = pandas.read_csv('../data/BPMED_electrodialysis.csv', header=0, names=['acid_flow', 'ph', 'eff', 'co2_flow', 'energy'], delimiter=',')

sw_flow = 2*df.acid_flow.values
co2_flow = df.co2_flow.values
flow_rate_ratio = co2_flow/sw_flow

ph = df.ph.values

##############################################################################
# Flow rate

sort_ind = np.argsort(flow_rate_ratio)
ph_fr = ph[sort_ind]
flow_rate_ratio = flow_rate_ratio[sort_ind]

w = np.ones_like(ph)
w[4] = 0
p1, residuals, _, _, _ = np.polyfit(flow_rate_ratio, ph_fr, 2, w=w, full=True)

flow_rate_ratio_fit = np.linspace(flow_rate_ratio[0], flow_rate_ratio[-1], 100)
fit1 = np.polyval(p1, flow_rate_ratio_fit)

##############################################################################
# Energy calculate

e = df.energy.values

sort_ind = np.argsort(ph)
ph = ph[sort_ind]
e = e[sort_ind]

p = np.polyfit(ph, e, 2)
ph_fit = np.linspace(ph[0], ph[-1], 100)
fit = np.polyval(p, ph_fit)

min_ph = -p[1]/2/p[0]

##############################################################################
# plot

fig, ax = plt.subplots(1, 2, figsize=(16, 6), dpi=400)

fsize = 20
tsize = 24

# plot energy

ax[0].scatter(ph, e)
ax[0].plot(ph_fit, fit, color='tab:red', label='Fitted quadratic')
ax[0].scatter(min_ph, np.polyval(p, min_ph), marker='x', color='black', label='Energy minimum')

ax[0].grid()

ax[0].tick_params(axis='both', labelsize=fsize-4)

ax[0].set_xlabel('pH', fontsize=fsize)
ax[0].set_ylabel('Energy $kJ/mol [CO_2]$', fontsize=fsize)

ax[0].set_title('Energy consumption fitting', fontsize=tsize, pad=20)

ax[0].legend(fontsize=fsize-4)

# plot flow rate fit

ax[1].scatter(flow_rate_ratio, ph_fr)
ax[1].plot(flow_rate_ratio_fit, fit1, color='tab:red', label='Fitted quadratic')

ax[1].grid()

ax[1].tick_params(axis='both', labelsize=fsize-4)

ax[1].set_ylabel('pH', fontsize=fsize)
ax[1].set_xlabel('$CO_2$ outflow $SL/min$ / Sea Water inflow $L/min$', fontsize=fsize)

ax[1].set_title('Normalized flow rate fitting', fontsize=tsize, pad=20)

ax[1].legend(fontsize=fsize-4)

fig.text(0.03, .94, '(a)', fontweight='bold', fontsize=tsize)
fig.text(0.51, .94, '(b)', fontweight='bold', fontsize=tsize)

fig.tight_layout()

fig.savefig('../plots/bpmed_fit.png')
