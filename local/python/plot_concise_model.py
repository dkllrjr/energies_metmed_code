import pyseafuel
import numpy as np
import matplotlib.pyplot as plt
from scipy import optimize
import system_run_seasonal as sr

# ──────────────────────────────────────────────────────────────────────────
# comparting McPhy Piel and the concise model

# electrolyzer parameters
# for Pt/Pt, a-H4SiW12O40/Ir, Pt/Ir
A = 250  # cm^2
E0 = [1.68, 1.4, 1.4]  # V
K = [9.95, 6.69, 27.8]  # 1/ohm cm^2
r = 0.15  # ohm cm^2
# i = 0  # setup selector

# ──────────────────────────────────────────────────────────────────────────

# flows = np.linspace(0, 2*sr.seawater_in_desalinator, 100)
flows = np.linspace(0, 0.001, 10000)

fig, ax = plt.subplots(1, 1, figsize=(6, 6), dpi=400)

fsize = 18
colors = ['tab:blue', 'tab:red', 'black']
types = ['Pt/Pt', '$\\alpha$-H$_4$SiW$_{12}$O$_{40}$/Ir', 'Pt/Ir']

for i in range(len(E0)):

    Vs = np.zeros_like(flows)
    Is = np.zeros_like(flows)

    for j, flowsj in enumerate(flows):
        # electrolyzer
        h2_mass_flow, _, power, V = pyseafuel.electrolyzer.Shen(flowsj, E0[i], K[i], r, A, output_voltage=True)
        # h2_mass_flow; kg/s
        # power_consumed_electrolyzer; W

        I = power / V  # current is very close to estimated calcs for the mcphy electrolyzer

        Vs[j] = V
        Is[j] = I

    ax.plot(Vs, Is/1e3, color=colors[i], label=types[i])
    
ax.set_xlim(1, Vs[-1])
ax.set_ylim(0, Is[-1]/1e3)

ax.set_xlabel('Cell Potential $V$', fontsize=fsize)
ax.set_ylabel('Cell Current $kA$', fontsize=fsize)

ax.set_title('Shen et al. Model', fontsize=fsize + 4)

ax.legend()

fig.tight_layout()
fig.savefig('../plots/concise_model.png')
