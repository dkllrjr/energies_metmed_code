import xarray as xr
import pandas as pd
import numpy as np
import med_sea_plotting as msp
import co2

##############################################################################

sst = xr.open_dataarray('../data/average_SST_1D_2D.nc')
psu = xr.open_dataarray('../data/average_SAL_1D_2D.nc')

lat = sst.lat
lon = sst.lon

##############################################################################
# sst

# var_min = np.nanmin(sst)
var_min = 16
var_max = np.nanmax(sst)
# var_max = 600

title = 'Sea Surface Temperature'
units = '$^\\circ C$'
plot_path = '../plots/sst_average.png'

msp.med_plot(sst, lat, lon, var_min, var_max, title, units, plot_path)

##############################################################################
# sst

# var_min = np.nanmin(psu)
var_min = 36
var_max = np.nanmax(psu)
# var_max = 600

title = 'Sea Surface Salinity'
units = '$PSU$'
plot_path = '../plots/salinity_average.png'

msp.med_plot(psu, lat, lon, var_min, var_max, title, units, plot_path)
