import pvlib
from datetime import datetime, timedelta
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import xarray as xr

##############################################################################
# assuming a module

sandia_modules = pvlib.pvsystem.retrieve_sam('SandiaMod')
sapm_inverters = pvlib.pvsystem.retrieve_sam('cecinverter')
module = sandia_modules['LG_LG290N1C_G3__2013_']
temperature_model_parameters = pvlib.temperature.TEMPERATURE_MODEL_PARAMETERS['sapm']['open_rack_glass_glass']

##############################################################################
# loaded atmospheric data

air_temp = xr.open_dataarray('../data/average_TAS.nc') - 273.15
uas = xr.open_dataarray('../data/average_UAS.nc').values
vas = xr.open_dataarray('../data/average_VAS.nc').values
ghi = xr.open_dataarray('../data/average_RSDS.nc').values

lat = air_temp.lat.values
lon = air_temp.lon.values

air_temp = air_temp.values

wind_speed = (uas**2 + vas**2)**.5

t_temp = []
beg = datetime(1993, 1, 1,  1, 30)
for i in range(58440):
    t_temp.append(beg + timedelta(hours=3*i))

t = pd.DatetimeIndex(t_temp)
t = t[len(t)//2]
# ghi = 400
# lat = 40
# lon = 5
# air_temp = 20
# wind_speed = 5

##############################################################################
# module array setup and irradiance calculations

solar_zenith = np.zeros_like(lat)
solar_azimuth = np.zeros_like(lat)
for i in range(lat.shape[0]):
    print(i)
    for j in range(lat.shape[1]):
        solpos = pvlib.solarposition.get_solarposition(t, lat[i, j], lon[i, j])
        solar_zenith[i, j] = np.mean(solpos.zenith)
        solar_azimuth[i, j] = np.mean(solpos.azimuth)

surface_tilt = lat
surface_azimuth = 180

aoi = pvlib.irradiance.aoi(surface_tilt, surface_azimuth, solar_zenith, solar_azimuth)

dni = pvlib.irradiance.disc(ghi, solar_zenith, t)
airmass = dni['airmass']
dni = dni['dni']

dhi = ghi - dni*np.cos(np.radians(solar_zenith))

poa_ground_diffuse = pvlib.irradiance.get_ground_diffuse(surface_tilt, ghi, surface_type='sea')
poa_sky_diffuse = pvlib.irradiance.get_sky_diffuse(surface_tilt, surface_azimuth, solar_zenith, solar_azimuth, dni, ghi, dhi, airmass=airmass)

irr = pvlib.irradiance.poa_components(aoi, dni, poa_sky_diffuse, poa_ground_diffuse)
poa_global = irr['poa_global']
poa_direct = irr['poa_direct']
poa_diffuse = irr['poa_diffuse']

am_abs = pvlib.atmosphere.get_absolute_airmass(airmass)

eff_irr = pvlib.pvsystem.sapm_effective_irradiance(poa_direct, poa_diffuse, am_abs, aoi, module)  # W/m2
cell_temp = pvlib.temperature.sapm_cell(poa_global, air_temp, wind_speed, **temperature_model_parameters)

dc = pvlib.pvsystem.sapm(eff_irr, cell_temp, module)

##############################################################################
# power results

potential_power = eff_irr  # W/m2
power = dc.p_mp/module.Area  # W/m2
