import med_sea_plotting as msp
import xarray as xr
import numpy as np

# ──────────────────────────────────────────────────────────────────────────

host_data = '../../../host_data/seafuel/ensemble/'

solar = xr.open_dataarray(host_data + 'solar_methanol_integrated_production.nc')
wind = xr.open_dataarray(host_data + 'turbine_methanol_integrated_production.nc')

lat = solar.lat.values
lon = solar.lon.values

data = [solar.values, wind.values, solar.values - wind.values]
var_min = [400, 0, -10]
var_max = [490, 450, 480]
titles = ['CH$_3$OH w/ Solar Panels', 'CH$_3$OH w/ Turbine', 'Difference (Solar Panels - Turbine)']
units = 'mL/m$^2$'
bot = [0.67, 0.355, 0.04]

# ──────────────────────────────────────────────────────────────────────────

plot_path = '../plots/integrated_methanol_production.png'
title_size = 24
font_size = 20

fig, ax = msp.fig_med_plot(3, 1, 18, 16)
ax = ax.flatten()
fig.suptitle('Integrated Methanol Production', y=.99, fontsize=title_size + 10)

cmaps = ['viridis', 'viridis', 'hot_r']

for i, axi in enumerate(ax):
    axi, med_map = msp.ax_med_plot(axi, data[i], lat, lon, var_min[i], var_max[i], titles[i], units, font_size, title_size, cmap=cmaps[i])

    # setting up the plot colorbar
    fig.subplots_adjust(right=0.8)
    cbar_ax = fig.add_axes([0.85, bot[i], 0.02, 0.25])
    cbar = fig.colorbar(med_map, cax=cbar_ax, orientation='vertical', pad=0.005)
    cbar.set_label(units, fontsize=font_size, labelpad=30)
    cbar.ax.tick_params(labelsize=font_size - 4, pad=10)

fig.text(0.05, 0.93, '(a)', fontsize=font_size-2, fontweight='bold')
fig.text(0.05, 0.625, '(b)', fontsize=font_size-2, fontweight='bold')
fig.text(0.05, 0.3, '(c)', fontsize=font_size-2, fontweight='bold')

# adjusting the plot to better fit the saved image area
fig.subplots_adjust(left=0.08, bottom=0.04, top=0.92, hspace=0.3)

msp.save_med_plot(fig, plot_path)
