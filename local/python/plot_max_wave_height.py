import xarray as xr
import med_sea_plotting as msp

##############################################################################
# wave height

data = xr.open_dataarray('../data/Max_Wave_Heights.nc')

lat = data.lat.values
long = data.lon.values

var_min = data.min()
var_max = data.max()
var_min = 1
var_max = 2.75

title = 'Max Wave Height'
units = '$m$'

# plot_path = '../plots/max_wave_height_average.png'
plot_path = '../plots/max_wave_height.png'

msp.med_plot(data.values, lat, long, var_min, var_max, title, units, plot_path, levels=15, cmap='Blues')
