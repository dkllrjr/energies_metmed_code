import windpowerlib

# ──────────────────────────────────────────────────────────────────────────
swt1203600 = {'turbine_type': 'SWT120/3600', 'hub_height': 87}  # london array wind farm
v902000 = {'turbine_type': 'V90/2000', 'hub_height': 100}

wt = windpowerlib.WindTurbine(**v902000)
wt = windpowerlib.WindTurbine(**swt1203600)
