import numpy as np

# ──────────────────────────────────────────────────────────────────────────

def find_lat_lon(loc,lat,lon):
    
    def archav(rad):
        return np.arccos(1 - 2*rad)

    def hav(rad):
        return (1 - np.cos(rad))/2

    def dist_sphere(r0, the0, phi0, r1, the1, phi1):
        return r0*archav(hav(np.abs(phi0-phi1)) + np.cos(phi0)*np.cos(phi1)*hav(np.abs(the0-the1)))
    
    def dist_sphere_deg(r0, the0, phi0, r1, the1, phi1):
        the0 = np.deg2rad(the0)
        phi0 = np.deg2rad(phi0)
        the1 = np.deg2rad(the1)
        phi1 = np.deg2rad(phi1)
        return dist_sphere(r0, the0, phi0, r1, the1, phi1)
    
    def lat_lon_near(lat_lon, loc):
        E_r = 6371000  # Earth's radius
        dist = []
        for i in range(len(lat_lon)):
            dist.append(dist_sphere_deg(E_r, lat_lon[i][1], lat_lon[i][0], E_r, loc[1], loc[0]))
        ind = np.where(np.array(dist) == np.min(np.array(dist)))[0]
        return np.array(lat_lon)[ind]

    lat_lon = []
    
    for i in range(lat.shape[0]):
        for j in range(lat.shape[1]):
            lat_lon.append([lat[i, j], lon[i, j]])
    
    ind_loc = []
    
    for i in range(len(loc)):
        ind_loc.append(lat_lon_near(lat_lon, loc[i]))
    
    lat_lon_np = np.array(lat_lon)
    lat_lon_np = lat_lon_np.reshape(lat.shape[0], lat.shape[1], 2)
    ind_loc_np = np.array(ind_loc)
    ind_loc_np = ind_loc_np.reshape(len(loc), 2)
    
    ind = []
    
    for k in range(ind_loc_np.shape[0]):
        for i in range(lat_lon_np.shape[0]):
            for j in range(lat_lon_np.shape[1]):
                if tuple(lat_lon_np[i, j]) == tuple(ind_loc_np[k]):
                    ind.append([i, j])
    
    ind = np.array(ind)
    
    return ind
