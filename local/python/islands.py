import numpy as np

# ──────────────────────────────────────────────────────────────────────────

swept_area = (120/2)**2 * np.pi

# ──────────────────────────────────────────────────────────────────────────

ec = 3954078 * 1e3 # kWh
print(ec)
methanol_eg_dens = 4.333  # kWh/L

l_methanol = ec / methanol_eg_dens  # L

l_methanol = np.round(l_methanol, 2)
print(l_methanol)

solar_prod = 465.10e-3  # L/m^2
wind_prod = 457.29e-3  # L/m^2

solar_area = l_methanol / solar_prod / 1e6  # km^2
wind_turb = l_methanol / wind_prod / swept_area  # no of turbines

# island area 8336 km^2

print('Crete', solar_area, wind_turb)

# ──────────────────────────────────────────────────────────────────────────
# balearic islands

ec = 111.02 / 3600 * 1e12  # kWh
print(ec)

l_methanol = ec / methanol_eg_dens  # L

l_methanol = np.round(l_methanol, 2)
print(l_methanol)

solar_prod = 448.68e-3  # L/m^2
wind_prod = 1.04e-3  # L/m^2

solar_area = l_methanol / solar_prod / 1e6  # km^2
wind_turb = l_methanol / wind_prod / swept_area  # no of turbines

print('Balearic', solar_area, wind_turb)
