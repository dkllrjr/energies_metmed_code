import pyseafuel
import numpy as np
import matplotlib.pyplot as plt
from scipy import optimize

# ──────────────────────────────────────────────────────────────────────────
# comparting McPhy Piel and the concise model

pure_water_flow = 0.0005  # L/s

h2_mass_flow_mcphy, _, power_mcphy = pyseafuel.electrolyzer.mcphy_piel(pure_water_flow)

# electrolyzer parameters
# for Pt/Pt, a-H4SiW12O49/Ir, Pt/Ir
A = 250  # cm^2
E0 = [1.68, 1.4, 1.4]  # V
K = [9.95, 6.69, 27.8]  # 1/ohm cm^2
r = 0.15  # ohm cm^2
i = 0  # setup selector

# electrolyzer
h2_mass_flow_concise, _, power_Shen, V = pyseafuel.electrolyzer.Shen(pure_water_flow, E0[i], K[i], r, A, output_voltage=True)
# h2_mass_flow; kg/s
# power_consumed_electrolyzer; W

I = power_Shen / V  # current is very close to estimated calcs for the mcphy electrolyzer

print(power_mcphy, power_Shen)

