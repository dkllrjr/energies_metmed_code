import xarray as xr
from glob import glob
import windpowerlib
import numpy as np

# ──────────────────────────────────────────────────────────────────────────
#turbine

swt1203600 = {'turbine_type': 'SWT120/3600', 'hub_height': 87}  # london array wind farm
wt = windpowerlib.WindTurbine(**swt1203600)
# v902000 = {'turbine_type': '', 'hub_height': 87}
# wt = windpowerlib.WindTurbine(**v902000)

# ──────────────────────────────────────────────────────────────────────────

# betz limit cp = Pow / (.5 * rho * A * U**3)
# Pow = cp * .5 * rho * A * U**3

cp = 16/27

def virtual_temp(T, r):
    return T*(1 + r/0.622)/(1 + r)

def moist_rho(Tv, P):
    return P/287.053/Tv

def water_ratio(q):
    return q/(1 - q)

def wind_speed(u, v):
    return (u**2 + v**2)**.5

def power_law(Ur, z, zr):
    # a = 1/7
    a = 0.11  # from offshore wind turbine
    return Ur * (z / zr)**a

def betz(rho, swept_area, U):
    cp = 16/27
    return cp * .5 * rho * swept_area * U**3

def betz_per_swept_area(rho, U):
    return cp * .5 * rho * U**3

def turbine_power_curve(wt, U):
    A = (wt.rotor_diameter/2)**2 * np.pi
    return np.where(U <= wt.power_curve.wind_speed.values[-1], np.interp(U, wt.power_curve.wind_speed.values, wt.power_curve.value.values), 0)/A

# ──────────────────────────────────────────────────────────────────────────

host_path = '../../../host_data/seafuel/seasonal/'

uas = xr.open_dataarray(host_path + 'seasonal_average_UAS.nc')
vas = xr.open_dataarray(host_path + 'seasonal_average_VAS.nc')
tas = xr.open_dataarray(host_path + 'seasonal_average_TAS.nc')
huss = xr.open_dataarray(host_path + 'seasonal_average_HUSS.nc')

# uas_std = xr.open_dataarray(host_path + 'seasonal_std_UAS.nc')
# vas_std = xr.open_dataarray(host_path + 'seasonal_std_VAS.nc')
# tas_std = xr.open_dataarray(host_path + 'seasonal_std_TAS.nc')
# huss_std = xr.open_dataarray(host_path + 'seasonal_std_HUSS.nc')

# ──────────────────────────────────────────────────────────────────────────

betz_power = uas.copy(deep=True)
turbine_power = uas.copy(deep=True)

# betz_power_std = uas.copy(deep=True)
# turbine_power_std = uas.copy(deep=True)

psl = 101325
for i in range(4):

    ws = wind_speed(uas[i], vas[i])
    r = water_ratio(huss[i])
    Tv = virtual_temp(tas[i], r)
    rho = moist_rho(Tv, psl)

    ws100 = power_law(ws, 87, 10)
    # ws100 = power_law(ws, 100, 10)

    betz_power[i] = betz_per_swept_area(rho, ws100)
    turbine_power[i] = turbine_power_curve(wt, ws100)

    # std

    # ws_std = wind_speed(uas_std[i], vas_std[i])
    # r = water_ratio(huss_std[i])
    # Tv = virtual_temp(tas_std[i], r)
    # rho = moist_rho(Tv, psl)

    # ws100 = power_law(ws, 100, 10)

    # betz_power[i] = betz_per_swept_area(rho, ws100)
    # turbine_power[i] = turbine_power_curve(wt, ws100)

path_str = host_path + 'seasonal_average_BETZ.nc'
betz_power.to_netcdf(path_str)
print('saved ' + path_str)

path_str = host_path + 'seasonal_average_TURB.nc'
turbine_power.to_netcdf(path_str)
print('saved ' + path_str)

# ──────────────────────────────────────────────────────────────────────────

host_path = '../../../host_data/seafuel/ensemble/'

uas = xr.open_dataarray(host_path + 'ensemble_average_UAS.nc')
vas = xr.open_dataarray(host_path + 'ensemble_average_VAS.nc')
tas = xr.open_dataarray(host_path + 'ensemble_average_TAS.nc')
huss = xr.open_dataarray(host_path + 'ensemble_average_HUSS.nc')

# ──────────────────────────────────────────────────────────────────────────

betz_power = uas.copy(deep=True)
turbine_power = uas.copy(deep=True)

psl = 101325

for i in range(betz_power.shape[0]):
    ws = wind_speed(uas[i], vas[i])
    r = water_ratio(huss[i])
    Tv = virtual_temp(tas[i], r)
    rho = moist_rho(Tv, psl)

    ws100 = power_law(ws, 100, 10)

    betz_power[i] = betz_per_swept_area(rho, ws100)
    turbine_power[i] = turbine_power_curve(wt, ws100)

path_str = host_path + 'ensemble_average_BETZ.nc'
betz_power.to_netcdf(path_str)
print('saved ' + path_str)

path_str = host_path + 'ensemble_average_TURB.nc'
turbine_power.to_netcdf(path_str)
print('saved ' + path_str)
