import xarray as xr
import med_sea_plotting as msp
import numpy as np
import matplotlib.pyplot as plt

import system_analyze_seasonal as system_run
import spherical_interpolation as sp

methanols = system_run.methanols
power_arr = system_run.power_arr

# ──────────────────────────────────────────────────────────────────────────

host_data = '../../../host_data/seafuel/seasonal/'

data = xr.open_dataarray(host_data + 'seasonal_average_SST_1D_2D.nc')

lat = data.lat.values
lon = data.lon.values

# ──────────────────────────────────────────────────────────────────────────

titles = ['DJF', 'MAM', 'JJA', 'SON']

# font sizes for the plot
font_size = 28
title_size = 34

# ──────────────────────────────────────────────────────────────────────────
# methanol production efficiency - NOT ENOUGH VARIANCE TO PLOT

vdata = []
for i in methanols:
    vdata.append(i['efficiency'] * 86400 * 1e6)  # muL/day / W 

prod_eff = vdata

# ──────────────────────────────────────────────────────────────────────────
# methanol on solar

solar = xr.open_dataarray(host_data + 'seasonal_average_POW_2.nc')

solar_lat = solar.lat.values
solar_lon = solar.lon.values

solar = solar.values

vdata = []
for i, methanol in enumerate(methanols):
    pv = sp.sphere_interpolation(solar[i], solar_lat, solar_lon, lat, lon)
    vdata.append(methanol['efficiency'] * pv * 86400 * 1e3)

soltemp = vdata

var_min = 0.9
var_max = 1.45

suptitle = 'Methanol Production w/ Solar'
units = '$\\frac{mL/day}{m^2}$'

plot_path = '../plots/methanol_on_solar_seasonal.png'

msp.med_plot_seasonal(vdata, lat, lon, var_min, var_max, titles, units, suptitle, font_size, title_size, plot_path, cmap='gnuplot')

print('plotted solar')

# ──────────────────────────────────────────────────────────────────────────
# turb

turb = xr.open_dataarray(host_data + 'seasonal_average_turb.nc')

turb = turb.values

vdata = []
for i, methanol in enumerate(methanols):
    turbi = sp.sphere_interpolation(turb[i], solar_lat, solar_lon, lat, lon)
    vdata.append(methanol['efficiency'] * turbi * 86400 * 1e3)

var_min = 0.01
var_max = 3

suptitle = 'Methanol Production w/ Wind'
units = '$\\frac{mL/day}{m^2}$'

plot_path = '../plots/methonal_on_turbine_seasonal.png'

msp.med_plot_seasonal(vdata, lat, lon, var_min, var_max, titles, units, suptitle, font_size, title_size, plot_path, cmap='gnuplot')

print('plotted wind')

# ──────────────────────────────────────────────────────────────────────────
# power requirements

# def absolute_value(val):
    # return np.round(val/100.*powers.sum(), 2)

# tsize = 22

total = []
degas = []
desal = []
electro = []
for i in power_arr:
    total.append(i['total'])
    degas.append(i['degasser'])
    desal.append(i['desalinator'])
    electro.append(i['electrolyzer'])

total = np.nanmean(total)
degas = np.nanmean(degas)
desal = np.nanmean(desal)
electro = np.nanmean(electro)

print(degas/total, desal/total, electro/total)
print(degas, desal, electro)

# powers = np.array([degas, desal, electro])/1e3

# fig, ax = plt.subplots(1, 1, figsize=(6, 5), dpi=400)

# ax.pie(powers, explode=[1, 1, 1], autopct=absolute_value, labels=['Degasser', 'Desalinator', 'Electrolyzer'], textprops={'fontsize':14})

# fig.suptitle('Power Consumption $kW$', fontsize=tsize, y = .925)

# fig.tight_layout()

# fig.savefig('../plots/power_pie.png')

