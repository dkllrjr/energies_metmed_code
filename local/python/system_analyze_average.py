import system_run_average as system_run
import numpy as np

# ──────────────────────────────────────────────────────────────────────────

flows, power, misc = system_run.flows, system_run.power, system_run.misc

# ──────────────────────────────────────────────────────────────────────────

seawater_in = flows['degasser']['seawater_in'] + flows['desalinator']['seawater_in']  # L/s
seawater_ratio = flows['desalinator']['seawater_in'] / flows['degasser']['seawater_in']  # volumetric raw feed input ratio

# methanol production
methanol = {}
methanol['ratio'] = flows['reactor']['ch3oh_out'] / seawater_in  # L/s / L/s
methanol['efficiency'] = flows['reactor']['ch3oh_out'] / power['total']  # L/s / W

# ──────────────────────────────────────────────────────────────────────────
# daily production

barrels_from_liters = 1/158.987  # bbl/L; liters to barrels
rho_crude = 0.8  # kg/L; density of crude oil
rho_ch3oh = 0.7863  # kg/L; liquid methanol density
# tonnes are 1000 kg; kt = 10^6 kg

daily = {}
daily['removed_co2'] = flows['degasser']['co2_out'] * 86400 / 1000  # Mg/day or tonnes/day
daily['methanol'] = flows['reactor']['ch3oh_out'] * 86400  # L/day
daily['barrels_methanol'] = daily['methanol'] * barrels_from_liters  # bbl/day
daily['kilotonnes_methanol'] = daily['methanol'] * rho_ch3oh / 10**6  # kt/day
daily['seawater_in'] = seawater_in * 86400  # L/day
daily['power'] = np.nanmean(power['total']) * 86400 / 10**9  # GJ/day
