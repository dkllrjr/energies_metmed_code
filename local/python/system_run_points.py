# ──────────────────────────────────────────────────────────────────────────
# Script to run the simulated solar methanol island reactor for different 
# values of salinity and sea surface temperature
# ──────────────────────────────────────────────────────────────────────────

import pyseafuel
import xarray as xr
import numpy as np
import pandas as pd

import slice as sli

# ──────────────────────────────────────────────────────────────────────────

host_data = '../../../host_data/seafuel/ensemble/'

# wind
# - wind speed
# - betz limit
# - turbine

# solar
# - shortwave
# - eff irr
# - solar panel

# sea
# - sea temp
# - sea sal
# - dic
# - max wave height?

# methanol prod
# - solar
# - wind

# plots to choose
# - dic
# - solar panel power
# - turbine power
# - methanol prod solar
# - methanol prod wind

# ──────────────────────────────────────────────────────────────────────────

# cretan sea, gulf of lion, alboran sea, alexandria
plot_names = ['cretan_sea', 'gol', 'alboran_sea', 'levantine_sea', 'balearic_islands']
names = ['Cretan Sea ($35.30^\\circ N$ $26^\\circ E$)', 'Gulf of Lion ($42^\\circ N$ $4^\\circ E$)', 'Alboran Sea ($36^\\circ N$ $-3^\\circ E$)', 'Levantine Sea ($32^\\circ N$ $30^\\circ E$)', 'Balearic Islands ($39.5^\\circ N$ $3^\\circ E$)']

loc = [[35.30, 26], [42, 4], [36, -3], [32, 30], [39.5, 3]]

sst = xr.open_dataarray(host_data + 'ensemble_average_SST_1D_2D.nc')
psu = xr.open_dataarray(host_data + 'ensemble_average_SAL_1D_2D.nc')
sst += 273.15

turb = xr.open_dataarray(host_data + 'ensemble_average_TURB.nc')
solar = xr.open_dataarray(host_data + 'ensemble_average_POW_2.nc')

mwh = xr.open_dataarray('../data/Max_Wave_Heights.nc')

ind_sea = sli.find_lat_lon(loc, sst.lat.values, sst.lon.values)
ind_air = sli.find_lat_lon(loc, turb.lat.values, turb.lon.values)

# ──────────────────────────────────────────────────────────────────────────
# seawater parameters

S = psu.values
SST = sst.values

# ──────────────────────────────────────────────────────────────────────────
# feed flows

arm_ratio = 0.001
seawater_in_degasser = 10  # L/s
seawater_in_desalinator = arm_ratio * seawater_in_degasser  # L/s

# ──────────────────────────────────────────────────────────────────────────
# desalinator parameters

water_ratio = .5
salt_removal = .99
stages = 1

# ──────────────────────────────────────────────────────────────────────────
# electrolyzer parameters

# for Pt/Ir
A_stack = 250  # cm^2
E0 = 1.4  # V
K = 27.8  # 1/ohm cm^2
R = 0.15  # ohm cm^2

# ──────────────────────────────────────────────────────────────────────────
# reactor parameters

P = 60  # bars;
T = 180  # C

# ══════════════════════════════════════════════════════════════════════════
# run reactor for points

mwh_arr = []
solar_arr = []
turb_arr = []

for i, indi in enumerate(ind_air):
    mwh_arr.append(mwh[indi[0], indi[1]].values)
    solar_arr.append(solar[:, indi[0], indi[1]].values)
    turb_arr.append(turb[:, indi[0], indi[1]].values)

# ──────────────────────────────────────────────────────────────────────────

sst_arr = []
psu_arr = []

for i, indi in enumerate(ind_sea):
    psu_arr.append(S[:, indi[0], indi[1]])
    sst_arr.append(SST[:, indi[0], indi[1]])
    
# ──────────────────────────────────────────────────────────────────────────

flows_arr, power_arr, misc_arr = [], [], []
for i, _ in enumerate(ind_sea):

    degasser = {'type': 'bipolar_membrane', 'seawater_in': seawater_in_degasser}
    desalinator = {'type': 'electrodialysis', 'seawater_in': seawater_in_desalinator, 'seawater_S': psu_arr[i], 'seawater_T': sst_arr[i], 'water_ratio': water_ratio, 'salt_removal': salt_removal, 'stages': stages}
    electrolyzer = {'type': 'Shen', 'E0': E0, 'K': K, 'R': R, 'area': A_stack}
    reactor = {'type': 'plug_flow', 'P': P, 'T': T}

    flows, power, misc = pyseafuel.system.seafuel(degasser, desalinator, electrolyzer, reactor)

    flows_arr.append(flows)
    power_arr.append(power)
    misc_arr.append(misc)

# ──────────────────────────────────────────────────────────────────────────

pH = 8.06

co2 = pd.read_csv('../data/ensemble_co2_1993-2013.csv')
co2 = co2.co2

dco2, hco3, co3, dic = [], [], [], []
for i in range(len(sst_arr)):
    dco2.append(pyseafuel.dic.carbon_dioxide(co2[i], sst_arr[i], psu_arr[i], volume=False))  # mol/kg
    hco3.append(pyseafuel.dic.bicarbonate(sst_arr[i], psu_arr[i], dco2[i], pH))
    co3.append(pyseafuel.dic.carbonate(sst_arr[i], psu_arr[i], hco3[i], pH))

    dic.append(dco2[i] + hco3[i] + co3[i])
