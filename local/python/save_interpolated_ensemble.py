# ──────────────────────────────────────────────────────────────────────────
# Script to run the simulated solar methanol island reactor for different 
# values of salinity and sea surface temperature
# ──────────────────────────────────────────────────────────────────────────

import xarray as xr
import numpy as np
from dask.diagnostics import ProgressBar
import multiprocessing as mp

import spherical_interpolation as sp

# ──────────────────────────────────────────────────────────────────────────

def save_xarray(data, lat, lon, path):

    data_xr = xr.DataArray(data, coords=dict(lon=(['y', 'x'], lon), lat=(['y', 'x'], lat)), dims=['t', 'y', 'x'])

    delayed = data_xr.to_netcdf(path, compute=False)

    with ProgressBar():
        results = delayed.compute()


def sphere(data, old_lat, old_lon, lat, lon, result, i):
    arr = sp.sphere_interpolation(data, old_lat, old_lon, lat, lon)
    result.put((i, arr))
    print(i)


def run_sphere(pv, turb, solar, turbine, old_lat, old_lon, lat, lon):

    # pv

    results_buffer = mp.Manager().Queue() # this how you instantiate the process safe buffer
    pool = mp.Pool(mp.cpu_count())

    for i in range(pv.shape[0]):
        pool.apply_async(sphere, args=(solar[i], old_lat, old_lon, lat, lon, results_buffer, i))

    pool.close()
    pool.join()

    results = [] # normal list to store the data
    while not results_buffer.empty(): # here we get all of the results out of the process safe buffer, since they are hard to work with otherwise.
        results.append(results_buffer.get()) # appending each item from the buffer to the normal list
    results.sort()

    for i in range(pv.shape[0]):
        pv[i] = results[i][1]

    print('pv')

    # turbine

    results_buffer = mp.Manager().Queue() # this how you instantiate the process safe buffer
    pool = mp.Pool(mp.cpu_count())

    for i in range(pv.shape[0]):
        pool.apply_async(sphere, args=(turbine[i], old_lat, old_lon, lat, lon, results_buffer, i))

    pool.close()
    pool.join()

    results = [] # normal list to store the data
    while not results_buffer.empty(): # here we get all of the results out of the process safe buffer, since they are hard to work with otherwise.
        results.append(results_buffer.get()) # appending each item from the buffer to the normal list
    results.sort()

    for i in range(pv.shape[0]):
        turb[i] = results[i][1]

    return pv, turb

# ──────────────────────────────────────────────────────────────────────────

host_data = '../../../host_data/seafuel/ensemble/'

sst = xr.open_dataarray(host_data + 'ensemble_average_SST_1D_2D.nc')
sst += 273.15

lat = sst.lat.values
lon = sst.lon.values

turbine = xr.open_dataarray(host_data + 'ensemble_average_TURB.nc')
solar = xr.open_dataarray(host_data + 'ensemble_average_POW_2.nc')

turbine = turbine.values

solar_lat = solar.lat.values
solar_lon = solar.lon.values

solar = solar.values

# ──────────────────────────────────────────────────────────────────────────
# seawater parameters

SST = sst.values

# ──────────────────────────────────────────────────────────────────────────

pv = np.zeros(shape=(solar.shape[0], SST.shape[1], SST.shape[2]))
turb = np.zeros(shape=(solar.shape[0], SST.shape[1], SST.shape[2]))

if __name__ == '__main__':
    pv, turb = run_sphere(pv, turb, solar, turbine, solar_lat, solar_lon, lat, lon)

    print('finished interpolation')

    save_path = host_data + 'ensemble_average_POW_2_interp.nc'
    save_xarray(pv, lat, lon, save_path)

    save_path = host_data + 'ensemble_average_TURB_interp.nc'
    save_xarray(turb, lat, lon, save_path)

