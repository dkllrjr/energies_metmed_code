import matplotlib.pyplot as plt
import pandas
from datetime import datetime
import numpy as np

##############################################################################

# co2 data from https://gml.noaa.gov/ccgg/trends/gl_data.html

data = pandas.read_csv('../data/sans_header_co2_mm_gl.csv')

date = []
for i, _ in enumerate(data.year):
    date.append(datetime(data.year[i], data.month[i], 15))

date = np.array(date)

##############################################################################

fig, ax = plt.subplots(1, 1, figsize=(8, 6), dpi=600)

fsize = 16

ax.axvspan(datetime(1993, 7, 1), datetime(2013, 6, 30), facecolor='black', alpha=.2, label='Time Period Studied')

ax.plot(date, data.average, color='tab:red', linewidth=3)

ax.legend(fontsize=fsize-4)

ax.set_title('Global Average Atmospheric CO$_2$', fontsize=fsize+10, pad=20)

ax.set_xlabel('Date', fontsize=fsize)
ax.set_ylabel('CO$_2$ $ppm$', fontsize=fsize)

ax.tick_params(axis='both', labelsize=fsize-2)

ax.set_xlim(date[0], date[-1])

fig.tight_layout()

fig.savefig('../plots/global_co2_time_series.png')
