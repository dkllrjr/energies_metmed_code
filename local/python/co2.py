import pyseafuel
import xarray as xr
import pandas as pd
import numpy as np
import med_sea_plotting as msp

##############################################################################

host_data = '../../../host_data/seafuel/average/'

psu = xr.open_dataarray(host_data + 'average_SAL_1D_2D.nc')
lat = psu.lat.values
lon = psu.lon.values
psu = psu.values

sst = xr.open_dataarray(host_data + 'average_SST_1D_2D.nc').values
sst += 273.15

co2 = pd.read_csv('../data/average_co2_1993-2013.csv')
co2 = co2[co2.columns[-1]].values

dissolved_co2 = pyseafuel.dic.carbon_dioxide(co2, sst, psu)  # mol/l

mol_weight_co2 = 44.01  # g/mol
dissolved_co2_mass = dissolved_co2 * mol_weight_co2  # g/l
