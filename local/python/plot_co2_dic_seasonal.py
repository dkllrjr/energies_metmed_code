import pyseafuel
import xarray as xr
import pandas as pd
import numpy as np
import med_sea_plotting as msp
import co2
from glob import glob

# ──────────────────────────────────────────────────────────────────────────

host_data = '../../../host_data/seafuel/seasonal/'

sst = xr.open_dataarray(host_data + 'seasonal_average_SST_1D_2D.nc')
psu = xr.open_dataarray(host_data + 'seasonal_average_SAL_1D_2D.nc')

sst += 273.15

lat = sst.lat
long = sst.lon

co2 = pd.read_csv('../data/seasonal_co2_1993-2013.csv')
co2 = co2[co2.columns[-1]].values

mol_weight_co2 = 44.01  # g/mol
mol_weight_hco3 = 61.02  # g/mol
mol_weight_co3 = 60.01  # g/mol

dco2 = []
hco3 = []
co3 = []

gdco2 = []
ghco3 = []
gco3 = []

dic = []
gdic = []

pH = 8.01

mon = np.arange(4)
for i in mon:
    dco2.append(pyseafuel.dic.carbon_dioxide(co2[i], sst[i].values, psu[i].values, volume=False))  # mol/kg
    hco3.append(pyseafuel.dic.bicarbonate(sst[i], psu[i], dco2[i], pH))
    co3.append(pyseafuel.dic.carbonate(sst[i], psu[i], hco3[i], pH))

    dic.append(dco2[i] + hco3[i] + co3[i])

    gdco2.append(mol_weight_co2 * dco2[i])
    ghco3.append(mol_weight_hco3 * hco3[i])
    gco3.append(mol_weight_co3 * co3[i])

    gdic.append(gdco2[i] + ghco3[i] + gco3[i])

dic6 = []
for i in mon:
    dic6.append(dic[i]*1e6)

# ──────────────────────────────────────────────────────────────────────────
# plot

var_min = np.nanmin(dic6)
var_max = np.nanmax(dic6)
# var_min = 1980
# var_max = 2065

suptitle = 'Dissolved Inorganic Carbon'
titles = ['DJF', 'MAM', 'JJA', 'SON']
units = '$\\mu mol/kg$'

# font sizes for the plot
font_size = 28
title_size = 34

plot_path = '../plots/dic_seasonal.png'

msp.med_plot_seasonal(dic6, lat, long, var_min, var_max, titles, units, suptitle, font_size, title_size, plot_path)

