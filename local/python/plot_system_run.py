import system_run
import co2
import med_sea_plotting as msp
import spherical_interpolation as sp
import numpy as np
import xarray as xr
import matplotlib.pyplot as plt

##############################################################################
# data

solar = xr.open_dataarray('../data/average_EFF_IRR.nc')
# power = xr.open_dataarray('../data/average_POW.nc').values

lat = system_run.lat
lon = system_run.lon

##############################################################################
# methanol per watt

methanol_efficiency = system_run.methanol_efficiency * 1e9  # nL/s / W

var_min = np.nanmin(methanol_efficiency)
var_max = np.nanmax(methanol_efficiency)
# var_max = 92.1
var_max = 93.5

title = 'Methanol Production per Watt'
units = '$\\frac{nL/s}{W}$'
plot_path = '../plots/methanol_production_average.png'

msp.med_plot(methanol_efficiency, lat, lon, var_min, var_max, title, units, plot_path)

##############################################################################
# co2

degassed_co2_mass_flow = system_run.degassed_co2_mass_flow  # kg/s
dissolved_co2_mass = co2.dissolved_co2_mass / 1000 * system_run.total_flow_in  # kg/s

co2_storage = degassed_co2_mass_flow / dissolved_co2_mass  # s

##############################################################################
# methanol island size

solar_lat = solar.lat.values
solar_lon = solar.lon.values

pv = solar.values * .17  # pv efficiency of .17
pv = sp.sphere_interpolation(pv, solar_lat, solar_lon, lat, lon)

methanol_per_area = system_run.methanol_efficiency * pv * 1e6  # mu L/s / m^2

var_min = np.nanmin(methanol_per_area)
# var_max = np.nanmax(methanol_per_area)
var_max = 3.95

title = 'Methanol Production per Area'
units = '$\\frac{\\mu L/s}{m^2}$'
plot_path = '../plots/methanol_production_area_average.png'

msp.med_plot(methanol_per_area, lat, lon, var_min, var_max, title, units, plot_path) 

##############################################################################
# power requirements

def absolute_value(val):
    return np.round(val/100.*powers.sum(), 2)

powers = np.array([system_run.power['power_consumed_degasser'], np.nanmean(system_run.power['power_consumed_desalinator']), np.nanmean(system_run.power['power_consumed_electrolyzer'])])/1e3

tsize = 22

fig, ax = plt.subplots(1, 1, figsize=(6, 5), dpi=400)

ax.pie(powers, explode=[.05, .05, .05], autopct=absolute_value, labels=['Degasser', 'Desalinator', 'Electrolyzer'], textprops={'fontsize':14})

fig.suptitle('Power Consumption $kW$', fontsize=tsize, y = .925)

fig.tight_layout()

fig.savefig('../plots/power_pie.png')

