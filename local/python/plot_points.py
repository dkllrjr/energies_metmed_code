import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import simpson

import system_run_points as sr

# ──────────────────────────────────────────────────────────────────────────

def moving_mean(s, N):
    M = 2*N + 1
    window = np.ones(M)
    ns = np.convolve(s, window, 'same')/M

    for i in range(N + 1):
        ns[i] = np.mean(s[0:M])

    for i in range(1, N + 2):
        ns[-i] = np.mean(s[-M::])

    return ns

# ──────────────────────────────────────────────────────────────────────────

fsize = 18

t = np.arange(len(sr.sst_arr[0]))
t8 = np.linspace(0, len(t), len(sr.solar_arr[0]))

# names = ['DIC', 'Solar Panel', 'V90/2000', 'CH$_3$OH Production w/ Solar', 'CH$_3$OH Production w/ Wind']
# names = ['Solar Panel', 'SWT-3.6-120', 'CH$_3$OH Production w/ Solar', 'CH$_3$OH Production w/ Wind']

# n = -1  # 0 for including dic

# solar_prods = []
# wind_prods = []
# for i in range(len(sr.sst_arr)):

    # print('max wave height:', sr.mwh_arr[i])

    # # fig, ax = plt.subplots(5, 1, figsize=(10, 14), dpi=400)
    # fig, ax = plt.subplots(4, 1, figsize=(10, 14), dpi=400)

    # meth_eff = sr.flows_arr[i]['reactor']['ch3oh_out'] / sr.power_arr[i]['total'] * 86400 * 1e3  # mL/day / W
    # meth_eff_temp = np.interp(t8, t, meth_eff)

    # # # dic
    # # ax[0].plot(t, sr.dic[i]*1e6)
    # # ax[0].set_ylabel('$\\frac{\\mu mol}{kg}$', fontsize=fsize)

    # # solar
    # ax[n+1].plot(t8, moving_mean(sr.solar_arr[i], 8))
    # ax[n+1].set_ylabel('$\\frac{W}{m^2}$', fontsize=fsize)

    # # turbine
    # ax[n+2].plot(t8, sr.turb_arr[i])
    # ax[n+2].set_ylabel('$\\frac{W}{m^2}$', fontsize=fsize)

    # # methanol solar
    # solar_meth = meth_eff_temp * sr.solar_arr[i]
    # ax[n+3].plot(t8, moving_mean(solar_meth, 8))
    # ax[n+3].set_ylabel('$\\frac{mL/day}{m^2}$', fontsize=fsize)

    # solar_prod = simpson(solar_meth, t8)
    # solar_prods.append(solar_prod)

    # # methanol wind
    # wind_meth = meth_eff_temp * sr.turb_arr[i]
    # ax[n+4].plot(t8, wind_meth)
    # ax[n+4].set_ylabel('$\\frac{mL/day}{m^2}$', fontsize=fsize)

    # ax[n+4].set_xlabel('Date', fontsize=fsize)

    # wind_prod = simpson(wind_meth, t8)
    # wind_prods.append(wind_prod)

    # for j, axi in enumerate(ax):
        # axi.set_xlim(t[0], t[-1])
        # axi.set_title(names[j], fontsize=fsize+2)
        # axi.tick_params(axis='both', which='both', labelsize=fsize-4)

    # fig.suptitle(sr.names[i], y=0.99, fontsize=fsize+4)
    # fig.tight_layout()

    # fig.savefig('../plots/' + sr.plot_names[i] + '.png')

plt.close()

# ──────────────────────────────────────────────────────────────────────────

fsize = 18

N = 8 * 3
alpha = 0.9

fig, ax = plt.subplots(2, 2, figsize=(20, 9), dpi=400)

colors = ['tab:blue', 'k', 'tab:red', 'tab:green']

for i in range(len(sr.sst_arr) - 1):

    meth_eff = sr.flows_arr[i]['reactor']['ch3oh_out'] / sr.power_arr[i]['total'] * 86400 * 1e3  # mL/day / W
    meth_eff_temp = np.interp(t8, t, meth_eff)

    # power
    ax[0,0].plot(t8[::N], moving_mean(sr.solar_arr[i], 8)[::N], color=colors[i], label=sr.names[i], alpha=alpha)
    ax[0,1].plot(t8[::N], sr.turb_arr[i][::N], color=colors[i], alpha=alpha)

    # methanol
    solar_meth = meth_eff_temp * sr.solar_arr[i]
    wind_meth = meth_eff_temp * sr.turb_arr[i]
    ax[1,0].plot(t8[::N], moving_mean(solar_meth, 8)[::N], color=colors[i], alpha=alpha)
    ax[1,1].plot(t8[::N], wind_meth[::N], color=colors[i], alpha=alpha)

    for j, axi in enumerate(ax.flatten()):
        axi.set_xlim(t[0], t[-1])
        axi.tick_params(axis='both', which='both', labelsize=fsize-4)

ax[0,0].legend(ncol=2, fontsize=fsize-4)

ax[0,0].set_ylabel('Power $\\frac{W}{m^2}$', fontsize=fsize)
ax[1,0].set_ylabel('Methanol $\\frac{mL/day}{m^2}$', fontsize=fsize)

ax[1,0].set_xlabel('Date', fontsize=fsize)
ax[1,1].set_xlabel('Date', fontsize=fsize)

ax[0,0].set_title('Solar', fontsize=fsize+6)
ax[0,1].set_title('Wind', fontsize=fsize+6)

fig.tight_layout()
fig.savefig('../plots/all_points.png')

# ──────────────────────────────────────────────────────────────────────────

i = 4
solar_meth = meth_eff_temp * sr.solar_arr[i]
wind_meth = meth_eff_temp * sr.turb_arr[i]
solar_prod = simpson(solar_meth, t8)
wind_prod = simpson(wind_meth, t8)
