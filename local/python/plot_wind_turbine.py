import xarray as xr
from glob import glob
import windpowerlib
import numpy as np
import matplotlib.pyplot as plt

# ──────────────────────────────────────────────────────────────────────────
#turbine

swt1203600 = {'turbine_type': 'SWT120/3600', 'hub_height': 87}  # london array wind farm
wt = windpowerlib.WindTurbine(**swt1203600)
# v902000 = {'turbine_type': 'V90/2000', 'hub_height': 100}
# wt = windpowerlib.WindTurbine(**v902000)

# ──────────────────────────────────────────────────────────────────────────

past = np.arange(wt.power_curve.wind_speed.values[-1], wt.power_curve.wind_speed.values[-1] + 5, .5)
past = np.append(wt.power_curve.wind_speed.values[-1], past)
zeros = np.append(wt.power_curve.value.values[-1]/1e6, np.zeros(len(past)-1))

fsize = 18

fig, ax = plt.subplots(1, 1, figsize=(7, 4.5), dpi=400)

ax.plot(wt.power_curve.wind_speed, wt.power_curve.value/1e6)
ax.plot(past, zeros, color='tab:blue', linestyle='--')

# ax.set_xlim(0, wt.power_curve.wind_speed.values[-1])
ax.set_xlim(0, wt.power_curve.wind_speed.values[-1] + 4.5)

ax.set_xlabel('Wind speed $m/s$', fontsize=fsize)
ax.set_ylabel('Power $MW$', fontsize=fsize)

# ax.set_title('V90/2000 Turbine Power Curve', fontsize=fsize+6)
ax.set_title('SWT-3.6-120 Turbine Power Curve', fontsize=fsize+6)

ax.grid()
ax.tick_params(axis='both', labelsize=fsize-4)

fig.tight_layout()

fig.savefig('../plots/wind_turbine_power_curve.png')
plt.close()
