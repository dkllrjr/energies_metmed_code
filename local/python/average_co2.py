import pandas
from pathlib import Path
from datetime import datetime
import numpy as np

##############################################################################
# average

with open(Path('../data/sans_header_co2_mm_gl.csv'), 'r') as file:
    df = pandas.read_csv(file, sep=',')

beg = datetime(1993, 7, 1, 12)
end = datetime(2013, 6, 30, 12)

ind = []

for i in range(len(df)):
    if beg <= datetime(df.year[i], df.month[i], 15, 12) <= end:
        ind.append(i)

ave = np.mean(df.average.iloc[ind])

df_ave = pandas.DataFrame({'1993 to 2013 average CO2': ave}, index=[0])
df_ave.to_csv('../data/average_co2_1993-2013.csv')

##############################################################################
# ensemble average

beg = datetime(1993, 7, 1, 12)
end = datetime(2013, 6, 30, 12)

ind = []

for i in range(12):
    ind.append([])

for i in range(len(df)):
    if beg <= datetime(df.year[i], df.month[i], 15, 12) <= end:
        ind[df.month[i]-1].append(i)

ensemble_ave = np.zeros(12)

for i in range(12):
    ensemble_ave[i] = np.mean(df.average.iloc[ind[i]])

months = []
for i in range(12):
    months.append(datetime(1993, i+1, 1, 0).strftime('%b'))

df_ensemble = pandas.DataFrame({'months': months, 'co2': ensemble_ave})
df_ensemble.to_csv('../data/ensemble_co2_1993-2013.csv')

##############################################################################
# seasonal average

beg = datetime(1993, 7, 1, 12)
end = datetime(2013, 6, 30, 12)

seasons = [[12, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]

ind = [[], [], [], []]
for i in range(len(df)):
    for j, season in enumerate(seasons):
        if beg <= datetime(df.year[i], df.month[i], 15, 12) <= end:
            if df.month[i] in season:
                ind[j].append(i)

seasonal_ave = np.zeros(4)

for i, season in enumerate(seasons):
    seasonal_ave[i] = np.mean(df.average.iloc[ind[i]])

seasons = ['DJF', 'MAM', 'JJA', 'SON']

df_seasonal = pandas.DataFrame({'seasons': seasons, 'co2': seasonal_ave})
df_seasonal.to_csv('../data/seasonal_co2_1993-2013.csv')
