from pathlib import Path
import xarray as xr

data_path = Path('~/Data/PhD/seafuel_local/').expanduser()

x = xr.open_dataset(data_path / 'ensemble_average_EFF_IRR.nc')
