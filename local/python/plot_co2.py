import pyseafuel
import xarray as xr
import pandas as pd
import numpy as np
import med_sea_plotting as msp
import co2

##############################################################################


psu = xr.open_dataarray('../data/average_SAL_1D_2D.nc')
lat = psu.lat.values
lon = psu.lon.values
psu = psu.values

sst = xr.open_dataarray('../data/average_SST_1D_2D.nc').values

co2 = pd.read_csv('../data/average_co2_1993-2013.csv')
co2 = co2[co2.columns[-1]].values

sst += 273.15
pH = 8.06

dco2 = pyseafuel.dic.carbon_dioxide(co2, sst, psu, volume=False)
hco3 = pyseafuel.dic.bicarbonate(sst, psu, dco2, pH)
co3 = pyseafuel.dic.carbonate(sst, psu, hco3, pH)

dic = dco2 + hco3 + co3
dic *= 1e6

##############################################################################
# plot

var_min = np.nanmin(dic)
var_max = np.nanmax(dic)
var_min = 1990

title = 'Dissolved Inorganic Carbon'
units = '$\\mu mol/kg$'
plot_path = '../plots/dic_med_average.png'

msp.med_plot(dic, lat, lon, var_min, var_max, title, units, plot_path)
