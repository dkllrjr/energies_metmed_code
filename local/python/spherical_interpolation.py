import numpy as np
from scipy.interpolate import griddata

##############################################################################

def sphere2plane(lat, lon, central_lat, central_lon):

    theta_0 = np.deg2rad(central_lon)
    phi_1 = np.deg2rad(central_lat)
    theta = np.deg2rad(lon)
    phi = np.deg2rad(lat)
    
    R = 6371e3

    k = 2 * R / (1 + np.sin(phi_1) * np.sin(phi) + np.cos(phi_1) * np.cos(phi) * np.cos(theta - theta_0))

    x = k * np.cos(phi) * np.sin(theta - theta_0)
    y = k * (np.cos(phi_1) * np.sin(phi) - np.sin(phi_1) * np.cos(phi) * np.cos(theta - theta_0))

    return x, y


# def plane2sphere(x, y, central_lat, central_lon):

    # theta_0 = np.deg2rad(central_lon)
    # phi_1 = np.deg2rad(central_lat)

    # R = 6371e3

    # rho = (x**2 + y**2)**.5
    # c = 2 * np.arctan2(rho, 2*R)

    # phi = np.arcsin( np.cos(c) * np.sin(phi_1) + y * np.sin(c) * np.cos(phi_1) / rho )
    # theta = theta_0 + np.arctan2( x * np.sin(c), (rho * np.cos(phi_1) * np.cos(c) - y* np.sin(phi_1) * np.sin(c)))

    # lat = np.rad2deg(phi)
    # lon = np.rad2deg(theta)

    # return lat, lon


def sphere_interpolation(var, lat, lon, nlat, nlon):

    clat = np.mean(lat)
    clon = np.mean(lon)

    x, y = sphere2plane(lat, lon, clat, clon)
    xn, yn = sphere2plane(nlat, nlon, clat, clon)

    shape = nlat.shape

    x = x.flatten()
    y = y.flatten()
    xn = xn.flatten()
    yn = yn.flatten()
    var = var.flatten()

    nvar = griddata((x, y), var, (xn, yn), method='linear')
    
    nvar = nvar.reshape(shape)

    return nvar
