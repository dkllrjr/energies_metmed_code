##############################################################################
#   Created: Thu Jun  4 12:11:38 2020
#   Author: mach
# This file separates the 42 N 5 E grid point from the WRFORCH MEDCORDEX run
# to compare it to my filtered versions
#
##############################################################################

import os
import xarray as xr
from glob import glob
from dask.diagnostics import ProgressBar
import numpy as np

##############################################################################

def find_lat_lon(loc,lat,lon):

    def archav(rad):
        return np.arccos(1 - 2*rad)

    def hav(rad):
        return (1 - np.cos(rad))/2

    def dist_sphere(r0, the0, phi0, r1, the1, phi1):
        return r0*archav(hav(np.abs(phi0-phi1)) + np.cos(phi0)*np.cos(phi1)*hav(np.abs(the0-the1)))

    def dist_sphere_deg(r0, the0, phi0, r1, the1, phi1):
        the0 = np.deg2rad(the0)
        phi0 = np.deg2rad(phi0)
        the1 = np.deg2rad(the1)
        phi1 = np.deg2rad(phi1)
        return dist_sphere(r0, the0, phi0, r1, the1, phi1)
    
    def lat_lon_near(lat_lon, loc):
        E_r = 6371000  # Earth's radius
        dist = []
        for i in range(len(lat_lon)):
            dist.append(dist_sphere_deg(E_r, lat_lon[i][1], lat_lon[i][0], E_r, loc[1], loc[0]))
        ind = np.where(np.array(dist) == np.min(np.array(dist)))[0]
        return np.array(lat_lon)[ind]

    lat_lon = []
    
    for i in range(lat.shape[0]):
        for j in range(lat.shape[1]):
            lat_lon.append([lat[i, j], lon[i, j]])
    
    ind_loc = []
    
    for i in range(len(loc)):
        ind_loc.append(lat_lon_near(lat_lon, loc[i]))
    
    lat_lon_np = np.array(lat_lon)
    lat_lon_np = lat_lon_np.reshape(lat.shape[0], lat.shape[1], 2)
    ind_loc_np = np.array(ind_loc)
    ind_loc_np = ind_loc_np.reshape(len(loc), 2)
    
    ind = []
    
    for k in range(ind_loc_np.shape[0]):
        for i in range(lat_lon_np.shape[0]):
            for j in range(lat_lon_np.shape[1]):
                if tuple(lat_lon_np[i, j]) == tuple(ind_loc_np[k]):
                    ind.append([i, j])
    
    ind = np.array(ind)
    
    return ind

##############################################################################
# Setting the data paths

root_path = '/bdd/MEDI/workspaces/dkeller/SEAFUEL/WRF/'

T_path = glob(root_path + 'BulkCore/*TAS.nc')
T_path.sort()
U_path = glob(root_path + 'BulkCore/*UAS.nc')
U_path.sort()
V_path = glob(root_path + 'BulkCore/*VAS.nc')
V_path.sort()

for i, data_path in enumerate([T_path, U_path, V_path]):

    for j, path in enumerate(data_path):

        data = xr.open_dataset(path)

        if j == 0:

            loc = [[42,5]]
            lat = data.nav_lat_grid_M.values
            lon = data.nav_lon_grid_M.values

            inds = find_lat_lon(loc,lat,lon)
            print('Found indices')

        save_file_part = os.path.basename(path)[:-3]

        for k, ind in enumerate(inds):

            save_loc_part = '_' + str(loc[k][0]) + '_' + str(loc[k][1])

            save_path = root_path + 'Points/' + save_file_part + save_loc_part + '.nc'

            slice_data = data.isel(south_north=ind[0], west_east=ind[1])

            delayed = slice_data.to_netcdf(save_path, compute=False)

            print('Saving ', save_path)
            with ProgressBar():
                results = delayed.compute()

            print('Saved data')
