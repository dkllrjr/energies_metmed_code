import xarray as xr
from glob import glob
import numpy as np
from dask.diagnostics import ProgressBar

def max_wave_height(ws):
    return 2*(ws/1.94384/12.5)**2

##############################################################################

root_path = '/bdd/MEDI/workspaces/dkeller/SEAFUEL/WRF/'

u_paths = glob(root_path + 'BulkCore/*UAS.nc')
v_paths = glob(root_path + 'BulkCore/*VAS.nc')
u_paths.sort()
v_paths.sort()

##############################################################################
# averaging

u_paths = u_paths[3::]
v_paths = v_paths[3::]

for i, u_path in enumerate(u_paths):
    
    temp_uas = xr.open_dataset(u_paths[i]).uas
    temp_vas = xr.open_dataset(v_paths[i]).vas

    if i == 0:
        nav_lat = temp_uas.nav_lat_grid_M.values
        nav_lon = temp_uas.nav_lon_grid_M.values
    
    ws = (temp_uas**2 + temp_vas**2)**.5
    temp_mwh = max_wave_height(ws)

    mwh_xr = xr.DataArray(temp_mwh.values, coords=dict(time=(['t'], temp_mwh.Time.values), lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'])

    # saving

    save_path = root_path + 'Waves/'

    delayed = mwh_xr.to_netcdf(save_path + u_path[-11:-6] + 'MWH.nc', compute=False)

    print('Saving MWH')
    with ProgressBar():
        results = delayed.compute()
