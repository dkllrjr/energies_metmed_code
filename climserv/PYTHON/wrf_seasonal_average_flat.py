import xarray as xr
from glob import glob
import numpy as np
from dask.diagnostics import ProgressBar
import os
import pandas

##############################################################################

root_path = '/bdd/MEDI/workspaces/dkeller/SEAFUEL/WRF/'

t_paths = glob(root_path + 'BulkCore/*TAS.nc')
t_paths.sort()

pot_pow_paths = glob(root_path + 'Solar_Power/*EFF_IRR_FLAT.nc')
pow_paths = glob(root_path + 'Solar_Power/*POW_FLAT.nc')
pot_pow_paths.sort()
pow_paths.sort()

##############################################################################
# averaging

t_paths = t_paths[3::]

def run_per_time(months, t_paths, pot_pow_paths, pow_paths):

    for i, t_path in enumerate(t_paths):
        
        temp_tas = xr.open_dataset(t_path).tas

        temp_pot_pow = xr.open_dataarray(pot_pow_paths[i])
        temp_pow = xr.open_dataarray(pow_paths[i])

        time = temp_tas.Time
        tind = []
        for j, t in enumerate(time):
            if pandas.Timestamp(t.values).month in months:
                tind.append(j)

        if i == 0:
            nav_lat = temp_tas.nav_lat_grid_M
            nav_lon = temp_tas.nav_lon_grid_M

            pot_pow = temp_pot_pow[tind].values
            pows = temp_pow[tind].values

        else:
            pot_pow = np.append(pot_pow, temp_pot_pow[tind].values, axis=0)
            pows = np.append(pows, temp_pow[tind].values, axis=0)

    pot_pow_ave = np.nanmean(pot_pow, axis=0)
    pow_ave = np.nanmean(pows, axis=0)

    pot_pow_std = np.nanstd(pot_pow, axis=0)
    pow_std = np.nanstd(pows, axis=0)

    return pot_pow_ave, pot_pow_std, pow_ave, pow_std, nav_lat, nav_lon

pot_pow_ave, pot_pow_std = [], []
pow_ave, pow_std = [], []

# DJF, MAM, JJA, SON
monthss = [[12, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]

for months in monthss:

    temp_pot_pow_ave, temp_pot_pow_std, temp_pow_ave, temp_pow_std, nav_lat, nav_lon = run_per_time(months, t_paths, pot_pow_paths, pow_paths)

    pot_pow_ave.append(temp_pot_pow_ave)
    pot_pow_std.append(temp_pot_pow_std)

    pow_ave.append(temp_pow_ave)
    pow_std.append(temp_pow_std)

pot_pow_ave = np.array(pot_pow_ave)
pot_pow_std = np.array(pot_pow_std)

pow_ave = np.array(pow_ave)
pow_std = np.array(pow_std)

##############################################################################
# ave

pot_pow_xr = xr.DataArray(pot_pow_ave, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'], attrs=dict(description="seasonal averaged data from 1993 to 2013"))

pow_xr = xr.DataArray(pow_ave, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'], attrs=dict(description="seasonal averaged data from 1993 to 2013"))

# std

pot_pow_std_xr = xr.DataArray(pot_pow_std, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'], attrs=dict(description="standard deviation for the seasonal averaged data from 1993 to 2013"))

pow_std_xr = xr.DataArray(pow_std, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'], attrs=dict(description="standard deviation for the seasonal averaged data from 1993 to 2013"))

##############################################################################
# saving

save_path = root_path + 'Seasonal/'

delayed = pot_pow_xr.to_netcdf(save_path + 'seasonal_average_EFF_IRR_FLAT.nc', compute=False)

print('Saving EFF IRR')
with ProgressBar():
    results = delayed.compute()

delayed = pow_xr.to_netcdf(save_path + 'seasonal_average_POW_FLAT.nc', compute=False)

print('Saving POW')
with ProgressBar():
    results = delayed.compute()

delayed = pot_pow_std_xr.to_netcdf(save_path + 'seasonal_std_EFF_IRR_FLAT.nc', compute=False)

print('Saving EFF IRR')
with ProgressBar():
    results = delayed.compute()

delayed = pow_std_xr.to_netcdf(save_path + 'seasonal_std_POW_FLAT.nc', compute=False)

print('Saving POW')
with ProgressBar():
    results = delayed.compute()
