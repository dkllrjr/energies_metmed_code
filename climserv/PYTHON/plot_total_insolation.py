# Calculate and plot the average solar insolation per year over the Med. Sea
# By Doug Keller

import xarray as xr
import numpy as np

import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from mpl_toolkits.axes_grid1 import make_axes_locatable

from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import cartopy.crs as ccrs
import cartopy.feature as cfeature


##############################################################################

def med_plot(var_grid, lat_grid, long_grid, var_min, var_max, plot_path):
    """Plots a 2D map of a scalar variable in the Mediterranean Sea.

    Parameters
    ----------
    var_grid : 2D array_like
        2D array of a scalar variable

    lat_grid : 2D array_like
        2D array of the latitude points corresponding to the locations of `var_grid`.

    long_grid : 2D array_like
        2D array of the longitude points corresponding to the locations of `var_grid`.

    var_min : float
        Scalar for desired minimum of `var_grid` to be sectioned and plotted.

    var_max : float
        Scalar for desired maximum of `var_grid` to be sectioned and plotted.

    plot_path : string
        Path to save the plot to.

    Notes
    -----
    This a sample program to plot a scalar in 2 dimensions.
    """

    # font sizes for the plot
    font_size = 28
    title_size = 34
    
    # initializing the plot; plt.subplots is just the easiest way I've found to make a plot with the flexibility I want
    fig, ax = plt.subplots(1, 1, figsize=(16, 7), dpi=200, subplot_kw={'projection': ccrs.PlateCarree(5)})

    # download coastline data at 10m resolution
    coast_10m = cfeature.NaturalEarthFeature('physical', 'land', '10m', edgecolor='black', facecolor='white') 
    
    # setting up the map plot
    gl = ax.gridlines(crs=ccrs.PlateCarree(), draw_labels=True, linewidth=.75, color='black', alpha=0.35, linestyle='--')  # setting up gridlines for lat. long.
    gl.xlabels_top = False  # removing upper and right lat. long. labels
    gl.ylabels_right = False
    gl.xlocator = mticker.FixedLocator([-90, -10, 0, 10, 20, 30, 90])  # sets the gridline locations
    gl.ylocator = mticker.FixedLocator([0, 30, 35, 40, 45, 90])
    gl.xformatter = LONGITUDE_FORMATTER  # formatter needed to get proper labels
    gl.yformatter = LATITUDE_FORMATTER
    gl.xlabel_style = {'size': font_size}  # setting label font size
    gl.ylabel_style = {'size': font_size}
    gl.xpadding = 20  # padding the lat. long. labels; sometimes they get squished into the plot
    gl.ypadding = 20
    
    ax.add_feature(coast_10m)  # adding coastline to plot
    ax.set_extent([-7, 37.25, 29.75, 46.25], crs=ccrs.PlateCarree())  # setting plot area
    
    levels = np.round(np.linspace(var_min, var_max, 20), 2)  # setting the levels of the filled contours
    med_map = ax.contourf(long_grid, lat_grid, var_grid, transform=ccrs.PlateCarree(), vmin=var_min, vmax=var_max, levels=levels, extend='both', cmap='gist_rainbow_r')  # plotting filled contours of the data

    # setting up the plot colorbar
    cbar = plt.colorbar(med_map, orientation='vertical', pad=0.025)
    cbar.set_label('$m^2/s^2$', fontsize=font_size - 4)
    cbar.ax.tick_params(labelsize=font_size - 4)

    med_map_contours = ax.contour(long_grid, lat_grid, var_grid, transform=ccrs.PlateCarree(), vmin=var_min, vmax=var_max, levels=levels)  # plotting contour lines of the same data; doesn't necessarily have to be the same data
    
    # inline labels for contour lines
    ax.clabel(med_map_contours, med_map_contours.levels, inline=True, fontsize=4)
    
    # setting title
    ax.set_title('Mediterranean Variable (Stratification Index)', pad=20, fontsize=title_size)

    # adjusting the plot to better fit the saved image area
    fig.subplots_adjust(top=0.90, left=0.09, right=1.04, bottom=0.1)

    # saving and closing the plot
    fig.savefig(plot_path)
    plt.close()


##############################################################################

# opening data
data = xr.open_dataarray('../data/NEMO-BULK-CONT_20130201_20130228_1D_SI.nc')

# extracting desire values for plotting
var_grid = data[15].values  # the dataset contains multiple days; we just want one
lat_grid = data.nav_lat_grid_W.values
long_grid = data.nav_lon_grid_W.values

# calling the plotting function
med_plot(var_grid, lat_grid, long_grid, 0, 3, '../plots/sample_med.png')
