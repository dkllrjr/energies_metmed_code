import xarray as xr
from glob import glob
import numpy as np
from dask.diagnostics import ProgressBar

def max_wave_height(ws):
    return 2*(ws/1.94384/12.5)**2

##############################################################################

root_path = '/bdd/MEDI/workspaces/dkeller/SEAFUEL/WRF/Waves/'

mwh_paths = glob(root_path + '*MWH.nc')
mwh_paths.sort()

##############################################################################
# max wave heights

mwh = []

for i, mwh_path in enumerate(mwh_paths):
    
    temp_mwh = xr.open_dataarray(mwh_path)

    if i == 0:
        nav_lat = temp_mwh.lat.values
        nav_lon = temp_mwh.lon.values

    mwh.append(np.nanmax(temp_mwh.values, axis=0))

mwh = np.array(mwh)
mwh = np.nanmax(mwh, axis=0)
    
mwh_xr = xr.DataArray(mwh, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['y', 'x'], attrs=dict(description='max wave heights over the 1993 to 2013 period based on experimental equations'))

# saving

save_path = root_path

delayed = mwh_xr.to_netcdf(save_path + 'Max_Wave_Heights.nc', compute=False)

print('Saving MWH')
with ProgressBar():
    results = delayed.compute()
