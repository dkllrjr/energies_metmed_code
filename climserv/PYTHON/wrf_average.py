import xarray as xr
from glob import glob
import numpy as np
from dask.diagnostics import ProgressBar

##############################################################################

root_path = '/bdd/MEDI/workspaces/dkeller/SEAFUEL/WRF/'

t_paths = glob(root_path + 'BulkCore/*TAS.nc')
u_paths = glob(root_path + 'BulkCore/*UAS.nc')
v_paths = glob(root_path + 'BulkCore/*VAS.nc')
sw_paths = glob(root_path + 'BulkCore/*RSDS.nc')
t_paths.sort()
u_paths.sort()
v_paths.sort()
sw_paths.sort()

pot_pow_paths = glob(root_path + 'Solar_Power/*EFF_IRR.nc')
pow_paths = glob(root_path + 'Solar_Power/*POW.nc')
pot_pow_paths.sort()
pow_paths.sort()

##############################################################################
# averaging

t_paths = t_paths[3::]
u_paths = u_paths[3::]
v_paths = v_paths[3::]
sw_paths = sw_paths[3::]

tas = []
uas = []
vas = []
sw = []
pot_pow = []
pows = []
date = []

for i, t_path in enumerate(t_paths):
    
    temp_tas = xr.open_dataset(t_path).tas
#    temp_uas = xr.open_dataset(u_paths[i]).uas
#    temp_vas = xr.open_dataset(v_paths[i]).vas
#    temp_sw = xr.open_dataset(sw_paths[i]).rsds

    temp_pot_pow = xr.open_dataarray(pot_pow_paths[i])
    temp_pow = xr.open_dataarray(pow_paths[i])

#    temp_sst = temp_sst.drop(['time_counter', 'time_centered'])

    if i == 0:
        nav_lat = temp_tas.nav_lat_grid_M
        nav_lon = temp_tas.nav_lon_grid_M

#    tas.append(np.nanmean(temp_tas.values, axis=0))
#    uas.append(np.nanmean(temp_uas.values, axis=0))
#    vas.append(np.nanmean(temp_vas.values, axis=0))
#    sw.append(np.nanmean(temp_sw.values, axis=0))

    pot_pow.append(np.nanmean(temp_pot_pow.values, axis=0))
    pows.append(np.nanmean(temp_pow.values, axis=0))

#tas = np.array(tas)
#uas = np.array(uas)
#vas = np.array(vas)
#sw = np.array(sw)

pot_pow = np.array(pot_pow)
pows = np.array(pows)

#tas_ave = np.nanmean(tas, axis=0)
#uas_ave = np.nanmean(uas, axis=0)
#vas_ave = np.nanmean(vas, axis=0)
#sw_ave = np.nanmean(sw, axis=0)

pot_pow_ave = np.nanmean(pot_pow, axis=0)
pow_ave = np.nanmean(pows, axis=0)

#tas_xr = xr.DataArray(tas_ave, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['y', 'x'], attrs=dict(description="averaged data from 1993 to 2013"))
#
#uas_xr = xr.DataArray(uas_ave, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['y', 'x'], attrs=dict(description="averaged data from 1993 to 2013"))
#
#vas_xr = xr.DataArray(vas_ave, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['y', 'x'], attrs=dict(description="averaged data from 1993 to 2013"))
#
#sw_xr = xr.DataArray(sw_ave, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['y', 'x'], attrs=dict(description="averaged data from 1993 to 2013"))

pot_pow_xr = xr.DataArray(pot_pow_ave, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['y', 'x'], attrs=dict(description="averaged data from 1993 to 2013"))

pow_xr = xr.DataArray(pow_ave, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['y', 'x'], attrs=dict(description="averaged data from 1993 to 2013"))

#tas_std = np.nanstd(tas, axis=0)
#uas_std = np.nanstd(uas, axis=0)
#vas_std = np.nanstd(vas, axis=0)
#sw_std = np.nanstd(sw, axis=0)

pot_pow_std = np.nanstd(pot_pow, axis=0)
pow_std = np.nanstd(pows, axis=0)

#tas_std_xr = xr.DataArray(tas_ave, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['y', 'x'], attrs=dict(description="standard deviation of the yearly means averaged from 1993 to 2013"))
#
#uas_std_xr = xr.DataArray(uas_ave, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['y', 'x'], attrs=dict(description="standard deviation of the yearly means averaged from 1993 to 2013"))
#
#vas_std_xr = xr.DataArray(vas_ave, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['y', 'x'], attrs=dict(description="standard deviation of the yearly means averaged from 1993 to 2013"))
#
#sw_std_xr = xr.DataArray(sw_ave, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['y', 'x'], attrs=dict(description="standard deviation of the yearly means averaged from 1993 to 2013"))

pot_pow_std_xr = xr.DataArray(pot_pow_std, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['y', 'x'], attrs=dict(description="averaged data from 1993 to 2013"))

pow_std_xr = xr.DataArray(pow_std, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['y', 'x'], attrs=dict(description="averaged data from 1993 to 2013"))

##############################################################################
# saving

save_path = root_path + 'Average/'

#delayed = tas_xr.to_netcdf(save_path + 'average_TAS.nc', compute=False)
#
#print('Saving TAS')
#with ProgressBar():
#    results = delayed.compute()
#
#delayed = uas_xr.to_netcdf(save_path + 'average_UAS.nc', compute=False)
#
#print('Saving UAS')
#with ProgressBar():
#    results = delayed.compute()
#
#delayed = vas_xr.to_netcdf(save_path + 'average_VAS.nc', compute=False)
#
#print('Saving VAS')
#with ProgressBar():
#    results = delayed.compute()
#
#delayed = sw_xr.to_netcdf(save_path + 'average_RSDS.nc', compute=False)
#
#print('Saving RSDS')
#with ProgressBar():
#    results = delayed.compute()

delayed = pot_pow_xr.to_netcdf(save_path + 'average_EFF_IRR.nc', compute=False)

print('Saving POT POW')
with ProgressBar():
    results = delayed.compute()

delayed = pow_xr.to_netcdf(save_path + 'average_POW.nc', compute=False)

print('Saving POW')
with ProgressBar():
    results = delayed.compute()

#delayed = tas_std_xr.to_netcdf(save_path + 'std_TAS.nc', compute=False)
#
#print('Saving TAS')
#with ProgressBar():
#    results = delayed.compute()
#
#delayed = uas_std_xr.to_netcdf(save_path + 'std_UAS.nc', compute=False)
#
#print('Saving UAS')
#with ProgressBar():
#    results = delayed.compute()
#
#delayed = vas_std_xr.to_netcdf(save_path + 'std_VAS.nc', compute=False)
#
#print('Saving VAS')
#with ProgressBar():
#    results = delayed.compute()
#
#delayed = sw_std_xr.to_netcdf(save_path + 'std_RSDS.nc', compute=False)
#
#print('Saving RSDS')
#with ProgressBar():
#    results = delayed.compute()

delayed = pot_pow_std_xr.to_netcdf(save_path + 'std_EFF_IRR.nc', compute=False)

print('Saving POT POW')
with ProgressBar():
    results = delayed.compute()

delayed = pow_std_xr.to_netcdf(save_path + 'std_POW.nc', compute=False)

print('Saving POW')
with ProgressBar():
    results = delayed.compute()
