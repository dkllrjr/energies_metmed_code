import xarray as xr
from glob import glob
import numpy as np
from dask.diagnostics import ProgressBar

##############################################################################

root_path = '/bdd/MEDI/workspaces/dkeller/SEAFUEL/NEMO/'

data_paths = glob(root_path + 'SolarIslands/DA/*2D.nc')
data_paths.sort()

##############################################################################
# averaging

data = []

sst = []
sal = []
date = []

for i, data_path in enumerate(data_paths):
    data.append(xr.open_dataset(data_path))
    
    temp_sst = data[-1].sosstsst
    date.append(np.mean(temp_sst.time_counter).values)
    temp_sst = temp_sst.drop(['time_counter', 'time_centered'])

    temp_sal = data[-1].sosaline
    temp_sal = temp_sal.drop(['time_counter', 'time_centered'])

    if i == 0:
        nav_lat = temp_sst.nav_lat
        nav_lon = temp_sst.nav_lon

    sst.append(np.nanmean(temp_sst.values, axis=0))
    sal.append(np.nanmean(temp_sal.values, axis=0))

sst = np.array(sst)
sal = np.array(sal)

sst_ave = np.nanmean(sst, axis=0)
sal_ave = np.nanmean(sal, axis=0)

sst_xr = xr.DataArray(sst_ave, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['y', 'x'], attrs=dict(description="averaged data from 1993 to 2013"))
sal_xr = xr.DataArray(sal_ave, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['y', 'x'], attrs=dict(description="averaged data from 1993 to 2013"))

sst_std = np.nanstd(sst, axis=0)
sal_std = np.nanstd(sal, axis=0)

sst_std_xr = xr.DataArray(sst_std, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['y', 'x'], attrs=dict(description="standard deviation of the yearly means averaged from 1993 to 2013"))
sal_std_xr = xr.DataArray(sal_std, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['y', 'x'], attrs=dict(description="standard deviation of the yearly means averaged from 1993 to 2013"))

##############################################################################
# saving

save_path = root_path + 'Average/'

delayed = sst_xr.to_netcdf(save_path + 'average_SST_1D_2D.nc', compute=False)

print('Saving SST')
with ProgressBar():
    results = delayed.compute()

delayed = sal_xr.to_netcdf(save_path + 'average_SAL_1D_2D.nc', compute=False)

print('Saving SAL')
with ProgressBar():
    results = delayed.compute()

delayed = sst_std_xr.to_netcdf(save_path + 'std_SST_1D_2D.nc', compute=False)

print('Saving SST')
with ProgressBar():
    results = delayed.compute()

delayed = sal_std_xr.to_netcdf(save_path + 'std_SAL_1D_2D.nc', compute=False)

print('Saving SAL')
with ProgressBar():
    results = delayed.compute()
