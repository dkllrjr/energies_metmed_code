import xarray as xr
from glob import glob
import numpy as np
from dask.diagnostics import ProgressBar

##############################################################################

root_path = '/bdd/MEDI/workspaces/dkeller/SEAFUEL/WRF/'

t_paths = glob(root_path + 'BulkCore/*TAS.nc')
u_paths = glob(root_path + 'BulkCore/*UAS.nc')
v_paths = glob(root_path + 'BulkCore/*VAS.nc')
sw_paths = glob(root_path + 'BulkCore/*RSDS.nc')
t_paths.sort()
u_paths.sort()
v_paths.sort()
sw_paths.sort()

pot_pow_paths = glob(root_path + 'Solar_Power/*EFF_IRR.nc')
pow_paths = glob(root_path + 'Solar_Power/*POW.nc')
pot_pow_paths.sort()
pow_paths.sort()

##############################################################################
# averaging

#t_paths = t_paths[1:3]
t_paths = t_paths[3::]
u_paths = u_paths[3::]
v_paths = v_paths[3::]
sw_paths = sw_paths[3::]

tas = []
uas = []
vas = []
sw = []
pot_pow = []
pows = []
tas_std = []
uas_std = []
vas_std = []
sw_std = []
pot_pow_std = []
pows_std = []
date = []

for i, t_path in enumerate(t_paths):
    
    temp_tas = xr.open_dataset(t_path).tas
    temp_uas = xr.open_dataset(u_paths[i]).uas
    temp_vas = xr.open_dataset(v_paths[i]).vas
    temp_sw = xr.open_dataset(sw_paths[i]).rsds
    date.append(np.mean(temp_tas.Time).values)

    temp_pot_pow = xr.open_dataarray(pot_pow_paths[i])
    temp_pow = xr.open_dataarray(pow_paths[i])

    if i == 0:
        nav_lat = temp_tas.nav_lat_grid_M
        nav_lon = temp_tas.nav_lon_grid_M

    tas.append(np.nanmean(temp_tas.values, axis=0))
    uas.append(np.nanmean(temp_uas.values, axis=0))
    vas.append(np.nanmean(temp_vas.values, axis=0))
    sw.append(np.nanmean(temp_sw.values, axis=0))
    
    pot_pow.append(np.nanmean(temp_pot_pow.values, axis=0))
    pows.append(np.nanmean(temp_pow.values, axis=0))
    
    tas_std.append(np.nanstd(temp_tas.values, axis=0))
    uas_std.append(np.nanstd(temp_uas.values, axis=0))
    vas_std.append(np.nanstd(temp_vas.values, axis=0))
    sw_std.append(np.nanstd(temp_sw.values, axis=0))
    
    pot_pow_std.append(np.nanstd(temp_pot_pow.values, axis=0))
    pows_std.append(np.nanstd(temp_pow.values, axis=0))

tas = np.array(tas)
uas = np.array(uas)
vas = np.array(vas)
sw = np.array(sw)
pot_pow = np.array(pot_pow)
pows = np.array(pows)

tas_std = np.array(tas_std)
uas_std = np.array(uas_std)
vas_std = np.array(vas_std)
sw_std = np.array(sw_std)
pot_pow_std = np.array(pot_pow_std)
pows_std = np.array(pows_std)

date = np.array(date)

tas_xr = xr.DataArray(tas, coords=dict(time=(['t'], date), lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'], attrs=dict(description="yearly averaged data from 1993 to 2013"))

uas_xr = xr.DataArray(uas, coords=dict(time=(['t'], date), lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'], attrs=dict(description="yearly averaged data from 1993 to 2013"))

vas_xr = xr.DataArray(vas, coords=dict(time=(['t'], date), lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'], attrs=dict(description="yearly averaged data from 1993 to 2013"))

sw_xr = xr.DataArray(sw, coords=dict(time=(['t'], date), lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'], attrs=dict(description="yearly averaged data from 1993 to 2013"))

pot_pow_xr = xr.DataArray(pot_pow, coords=dict(time=(['t'], date), lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'], attrs=dict(description="yearly averaged data from 1993 to 2013"))

pow_xr = xr.DataArray(pows, coords=dict(time=(['t'], date), lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'], attrs=dict(description="yearly averaged data from 1993 to 2013"))

tas_std_xr = xr.DataArray(tas_std, coords=dict(time=(['t'], date), lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'], attrs=dict(description="yearly standard deviation data from 1993 to 2013"))

uas_std_xr = xr.DataArray(uas_std, coords=dict(time=(['t'], date), lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'], attrs=dict(description="yearly standard deviation data from 1993 to 2013"))

vas_std_xr = xr.DataArray(vas_std, coords=dict(time=(['t'], date), lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'], attrs=dict(description="yearly standard deviation data from 1993 to 2013"))

sw_std_xr = xr.DataArray(sw_std, coords=dict(time=(['t'], date), lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'], attrs=dict(description="yearly standard deviation data from 1993 to 2013"))

pot_pow_std_xr = xr.DataArray(pot_pow_std, coords=dict(time=(['t'], date), lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'], attrs=dict(description="yearly standard deviation data from 1993 to 2013"))

pow_std_xr = xr.DataArray(pows_std, coords=dict(time=(['t'], date), lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'], attrs=dict(description="yearly standard deviation data from 1993 to 2013"))

##############################################################################
# saving

save_path = root_path + 'Yearly/'

#delayed = tas_xr.to_netcdf(save_path + 'yearly_average_TAS.nc', compute=False)
#
#print('Saving TAS')
#with ProgressBar():
#    results = delayed.compute()
#
#delayed = uas_xr.to_netcdf(save_path + 'yearly_average_UAS.nc', compute=False)
#
#print('Saving UAS')
#with ProgressBar():
#    results = delayed.compute()
#
#delayed = vas_xr.to_netcdf(save_path + 'yearly_average_VAS.nc', compute=False)
#
#print('Saving VAS')
#with ProgressBar():
#    results = delayed.compute()
#
#delayed = sw_xr.to_netcdf(save_path + 'yearly_average_RSDS.nc', compute=False)
#
#print('Saving RSDS')
#with ProgressBar():
#    results = delayed.compute()

delayed = pot_pow_xr.to_netcdf(save_path + 'yearly_average_EFF_IRR.nc', compute=False)

print('Saving POT POW')
with ProgressBar():
    results = delayed.compute()

delayed = pow_xr.to_netcdf(save_path + 'yearly_average_POW.nc', compute=False)

print('Saving POW')
with ProgressBar():
    results = delayed.compute()

# std

#delayed = tas_std_xr.to_netcdf(save_path + 'yearly_std_TAS.nc', compute=False)
#
#print('Saving TAS')
#with ProgressBar():
#    results = delayed.compute()
#
#delayed = uas_std_xr.to_netcdf(save_path + 'yearly_std_UAS.nc', compute=False)
#
#print('Saving UAS')
#with ProgressBar():
#    results = delayed.compute()
#
#delayed = vas_std_xr.to_netcdf(save_path + 'yearly_std_VAS.nc', compute=False)
#
#print('Saving VAS')
#with ProgressBar():
#    results = delayed.compute()
#
#delayed = sw_std_xr.to_netcdf(save_path + 'yearly_std_RSDS.nc', compute=False)
#
#print('Saving RSDS')
#with ProgressBar():
#    results = delayed.compute()

delayed = pot_pow_std_xr.to_netcdf(save_path + 'yearly_std_EFF_IRR.nc', compute=False)

print('Saving POT POW')
with ProgressBar():
    results = delayed.compute()

delayed = pow_std_xr.to_netcdf(save_path + 'yearly_std_POW.nc', compute=False)

print('Saving POW')
with ProgressBar():
    results = delayed.compute()
