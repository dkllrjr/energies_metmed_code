import xarray as xr
from glob import glob
import numpy as np
from dask.diagnostics import ProgressBar
import pandas
import os

##############################################################################

root_path = '/bdd/MEDI/workspaces/dkeller/SEAFUEL/NEMO/'

data_paths = glob(root_path + 'SolarIslands/DA/*2D.nc')
data_paths.sort()

##############################################################################
# averaging

def run_per_time(months, data_paths):

    data = []

    for i, data_path in enumerate(data_paths):

        data.append(xr.open_dataset(data_path))

        temp_sst = data[-1].sosstsst
        time = temp_sst.time_centered

        tind = []
        for j, t in enumerate(time):
            if pandas.Timestamp(t.values).month in months:
                tind.append(j)
        
        temp_sst = temp_sst.drop(['time_counter', 'time_centered'])
        temp_sal = data[-1].sosaline
        temp_sal = temp_sal.drop(['time_counter', 'time_centered'])

        if i == 0:
            nav_lat = temp_sst.nav_lat
            nav_lon = temp_sst.nav_lon

            sst = temp_sst[tind].values
            sal = temp_sal[tind].values

            print(sst.shape, sal.shape)
        else:
            sst = np.append(sst, temp_sst[tind].values, axis=0)
            sal = np.append(sal, temp_sal[tind].values, axis=0)

    sst_ave = np.nanmean(sst, axis=0)
    sal_ave = np.nanmean(sal, axis=0)

    sst_std = np.nanstd(sst, axis=0)
    sal_std = np.nanstd(sal, axis=0)

    return sst_ave, sst_std, sal_ave, sal_std, nav_lat, nav_lon

sst_ave, sst_std = [], []
sal_ave, sal_std = [], []

# DJF, MAM, JJA, SON
monthss = [[12, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]

for i, months in enumerate(monthss):

    temp_sst_ave, temp_sst_std, temp_sal_ave, temp_sal_std, nav_lat, nav_lon = run_per_time(months, data_paths)

    sst_ave.append(temp_sst_ave)
    sst_std.append(temp_sst_std)

    sal_ave.append(temp_sal_ave)
    sal_std.append(temp_sal_std)

sst_ave, sst_std = np.array(sst_ave), np.array(sst_std)
sal_ave, sal_std = np.array(sal_ave), np.array(sal_std)

print(sst_ave.shape)

sst_xr = xr.DataArray(sst_ave, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'], attrs=dict(description="average of the seasons from 1993 to 2013"))
sal_xr = xr.DataArray(sal_ave, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'], attrs=dict(description="average of the seasons from 1993 to 2013"))

sst_std_xr = xr.DataArray(sst_std, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'], attrs=dict(description="standard deviation of the seasonal averaged from 1993 to 2013"))
sal_std_xr = xr.DataArray(sal_std, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'], attrs=dict(description="standard deviation of the seasonal averaged from 1993 to 2013"))

##############################################################################
# saving

save_path = root_path + 'Seasonal/'

# sst

filename = save_path + 'seasonal_average_SST_1D_2D.nc'
if os.path.exists(filename):
    print(filename, 'skipped')
else:
    delayed = sst_xr.to_netcdf(filename, compute=False)

    print('Saving SST')
    with ProgressBar():
        results = delayed.compute()

# salinity

filename = save_path + 'seasonal_average_SAL_1D_2D.nc'
if os.path.exists(filename):
    print(filename, 'skipped')
else:
    delayed = sal_xr.to_netcdf(filename, compute=False)

    print('Saving SAL')
    with ProgressBar():
        results = delayed.compute()

# sst std

filename = save_path + 'seasonal_std_SST_1D_2D.nc'
if os.path.exists(filename):
    print(filename, 'skipped')
else:
    delayed = sst_std_xr.to_netcdf(filename, compute=False)

    print('Saving SST')
    with ProgressBar():
        results = delayed.compute()

# salinity std

filename = save_path + 'seasonal_std_SAL_1D_2D.nc'
if os.path.exists(filename):
    print(filename, 'skipped')
else:
    delayed = sal_std_xr.to_netcdf(filename, compute=False)

    print('Saving SAL')
    with ProgressBar():
        results = delayed.compute()
