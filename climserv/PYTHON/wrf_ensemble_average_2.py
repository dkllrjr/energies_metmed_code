import xarray as xr
from glob import glob
import numpy as np
from dask.diagnostics import ProgressBar
import os

##############################################################################

root_path = '/bdd/MEDI/workspaces/dkeller/SEAFUEL/WRF/'

t_paths = glob(root_path + 'BulkCore/*TAS.nc')
t_paths.sort()

pot_pow_paths = glob(root_path + 'Solar_Power/*EFF_IRR_2.nc')
pow_paths = glob(root_path + 'Solar_Power/*POW_2.nc')
pot_pow_paths.sort()
pow_paths.sort()

##############################################################################
# averaging

t_paths = t_paths[3::]

pot_pow = []
pows = []

for i, t_path in enumerate(t_paths):
    
    temp_tas = xr.open_dataset(t_path).tas

    temp_pot_pow = xr.open_dataarray(pot_pow_paths[i])
    temp_pow = xr.open_dataarray(pow_paths[i])

    if i == 0:
        nav_lat = temp_tas.nav_lat_grid_M
        nav_lon = temp_tas.nav_lon_grid_M

    if temp_tas.shape[0] < 2928:

        tpotpow = xr.full_like(temp_pot_pow[0:8], np.nan)
        pot_pow.append(xr.concat([temp_pot_pow[0:480], tpotpow, temp_pot_pow[480::]], dim='t').values)
        
        tpow = xr.full_like(temp_pow[0:8], np.nan)
        pows.append(xr.concat([temp_pow[0:480], tpow, temp_pow[480::]], dim='t').values)

        print('yes')
    else:
        pot_pow.append(temp_pot_pow.values)
        pows.append(temp_pow.values)

pot_pow = np.array(pot_pow)
pows = np.array(pows)

pot_pow_ave = np.nanmean(pot_pow, axis=0)
pow_ave = np.nanmean(pows, axis=0)

pot_pow_xr = xr.DataArray(pot_pow_ave, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'], attrs=dict(description="ensemble averaged data from 1993 to 2013"))

pow_xr = xr.DataArray(pow_ave, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'], attrs=dict(description="ensemble averaged data from 1993 to 2013"))

pot_pow_std = np.nanstd(pot_pow, axis=0)
pow_std = np.nanstd(pows, axis=0)

pot_pow_std_xr = xr.DataArray(pot_pow_std, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'], attrs=dict(description="standard deviation for the ensemble averaged data from 1993 to 2013"))

pow_std_xr = xr.DataArray(pow_std, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'], attrs=dict(description="standard deviation for the ensemble averaged data from 1993 to 2013"))

##############################################################################
# saving

save_path = root_path + 'Ensemble/'

delayed = pot_pow_xr.to_netcdf(save_path + 'ensemble_average_EFF_IRR_2.nc', compute=False)

print('Saving EFF IRR')
with ProgressBar():
    results = delayed.compute()

delayed = pow_xr.to_netcdf(save_path + 'ensemble_average_POW_2.nc', compute=False)

print('Saving POW')
with ProgressBar():
    results = delayed.compute()

delayed = pot_pow_std_xr.to_netcdf(save_path + 'ensemble_std_EFF_IRR_2.nc', compute=False)

print('Saving EFF IRR')
with ProgressBar():
    results = delayed.compute()

delayed = pow_std_xr.to_netcdf(save_path + 'ensemble_std_POW_2.nc', compute=False)

print('Saving POW')
with ProgressBar():
    results = delayed.compute()
