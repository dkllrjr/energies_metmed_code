import pvlib
from datetime import datetime
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import xarray as xr
from glob import glob
import multiprocessing as mp
from dask.diagnostics import ProgressBar

##############################################################################

def power_output(module, temperature_model_parameters, surface_tilt, surface_azimuth, t, lat, lon, ghi, air_temp, wind_speed):

    Wp = 1000

    solpos = pvlib.solarposition.get_solarposition(time=t, latitude=lat, longitude=lon)

    relative_airmass = pvlib.atmosphere.get_relative_airmass(zenith=solpos['zenith'], model='young1994')
    dni = pvlib.irradiance.dirint(ghi, solpos['zenith'], t, pressure=101325, use_delta_kt_prime=False)
    dhi = ghi - dni*np.cos(np.radians(solpos['zenith']))
    total_irr = pvlib.irradiance.get_total_irradiance(surface_tilt=surface_tilt, surface_azimuth=surface_azimuth,
                                                        solar_zenith=solpos['zenith'], solar_azimuth=solpos['azimuth'],
                                                        ghi=ghi, dni=dni, dhi=dhi, dni_extra='none',
                                                        airmass=relative_airmass, model='isotropic')

    absolute_airmass = pvlib.atmosphere.get_absolute_airmass(relative_airmass, pressure=101325)
    aoi = pvlib.irradiance.aoi(surface_tilt, surface_azimuth, solpos['zenith'], solpos['azimuth'])
    eff_irr = pvlib.pvsystem.sapm_effective_irradiance(poa_direct=total_irr.poa_direct, poa_diffuse=total_irr.poa_diffuse,
                                                       airmass_absolute=absolute_airmass, aoi=aoi, module=module)

    cell_temp = pvlib.temperature.sapm_cell(poa_global=total_irr.poa_global, temp_air=(air_temp-273.15), wind_speed=wind_speed, **temperature_model_parameters)

    dc = pvlib.pvsystem.pvwatts_dc(g_poa_effective=eff_irr, temp_cell=cell_temp, pdc0=Wp, gamma_pdc=-0.004)

    # power results

    potential_power = eff_irr  # W/m2
    power = dc/module.Area  # W/m2

    return power, potential_power

def power_output_loop(module, temperature_model_parameters, surface_tilt, surface_azimuth, t, lat, lon, ghi, air_temp, wind_speed):

    power = np.zeros_like(ghi)
    potential_power = np.zeros_like(ghi)

    for i in range(lat.shape[0]):
#        print(i)
        for j in range(lat.shape[1]):
            power[:, i, j], potential_power[:, i, j] = power_output(module, temperature_model_parameters, surface_tilt, surface_azimuth, t, lat[i, j].values, lon[i, j].values, ghi[:, i, j].values, air_temp[:, i, j].values, wind_speed[:, i, j].values)

    return power, potential_power

def run_power_output_loop(tas_path, uas_path, vas_path, sw_path, module, temperature_model_parameters, surface_azimuth):

    tas = xr.open_dataset(tas_path)
    uas = xr.open_dataset(uas_path)
    vas = xr.open_dataset(vas_path)
    sw = xr.open_dataset(sw_path)

    t = pd.DatetimeIndex(tas.Time.values)

    lat = tas.nav_lat_grid_M
    lon = tas.nav_lon_grid_M

    surface_tilt = 0

    ghi = sw.rsds
    air_temp = tas.tas
    wind_speed = (uas.uas**2 + vas.vas**2)**.5

    power, potential_power = power_output_loop(module, temperature_model_parameters, surface_tilt, surface_azimuth, t, lat, lon, ghi, air_temp, wind_speed)
    
    potential_power_xr = xr.DataArray(potential_power, coords=dict(time=(['t'], tas.Time), lon=(['y', 'x'], lon.values), lat=(['y', 'x'], lat.values)), dims=['t', 'y', 'x'])

    power_xr = xr.DataArray(power, coords=dict(time=(['t'], tas.Time), lon=(['y', 'x'], lon.values), lat=(['y', 'x'], lat.values)), dims=['t', 'y', 'x'])

    # saving

    save_path = root_path + 'Solar_Power/'

    delayed = potential_power_xr.to_netcdf(save_path + 'WRF_' + tas_path[-12:-7] + '_EFF_IRR_FLAT.nc', compute=False)

    print('Saving Potential Power')
    with ProgressBar():
        results = delayed.compute()

    delayed = power_xr.to_netcdf(save_path + 'WRF_' + tas_path[-12:-7] + '_POW_FLAT.nc', compute=False)

    print('Saving Power')
    with ProgressBar():
        results = delayed.compute()

##############################################################################
# assuming a module

sandia_modules = pvlib.pvsystem.retrieve_sam('SandiaMod')
sapm_inverters = pvlib.pvsystem.retrieve_sam('cecinverter')
module = sandia_modules['SunPower_128_Cell_Module___2009_']
temperature_model_parameters = pvlib.temperature.TEMPERATURE_MODEL_PARAMETERS['sapm']['open_rack_glass_glass']

##############################################################################
# loaded atmospheric data

root_path = '/bdd/MEDI/workspaces/dkeller/SEAFUEL/WRF/'

tas_paths = glob(root_path + 'BulkCore/*TAS.nc')
uas_paths = glob(root_path + 'BulkCore/*UAS.nc')
vas_paths = glob(root_path + 'BulkCore/*VAS.nc')
sw_paths = glob(root_path + 'BulkCore/*RSDS.nc')

tas_paths.sort()
uas_paths.sort()
vas_paths.sort()
sw_paths.sort()

tas_paths = tas_paths[3::]
uas_paths = uas_paths[3::]
vas_paths = vas_paths[3::]
sw_paths = sw_paths[3::]

pool = mp.Pool(10)
surface_azimuth = 180

for i, tas_path in enumerate(tas_paths):

    print(i, tas_path)
#    run_power_output_loop(tas_path, uas_paths[i], vas_paths[i], sw_paths[i], module, temperature_model_parameters, surface_azimuth)
    pool.apply_async(run_power_output_loop, args=(tas_path, uas_paths[i], vas_paths[i], sw_paths[i], module, temperature_model_parameters, surface_azimuth))
#    pool.apply(run_power_output_loop, args=(tas_path, uas_paths[i], vas_paths[i], sw_paths[i], module, temperature_model_parameters, surface_azimuth))

pool.close()
pool.join()
