import xarray as xr
from glob import glob
import numpy as np
from dask.diagnostics import ProgressBar
import os
import pandas

##############################################################################

root_path = '/bdd/MEDI/workspaces/dkeller/SEAFUEL/WRF/'

t_paths = glob(root_path + 'BulkCore/*TAS.nc')
u_paths = glob(root_path + 'BulkCore/*UAS.nc')
v_paths = glob(root_path + 'BulkCore/*VAS.nc')
sw_paths = glob(root_path + 'BulkCore/*RSDS.nc')
q_paths = glob(root_path + 'BulkCore/*HUSS.nc')
t_paths.sort()
u_paths.sort()
v_paths.sort()
sw_paths.sort()
q_paths.sort()

pot_pow_paths = glob(root_path + 'Solar_Power/*EFF_IRR.nc')
pow_paths = glob(root_path + 'Solar_Power/*POW.nc')
pot_pow_paths.sort()
pow_paths.sort()

##############################################################################
# averaging

t_paths = t_paths[3::]
u_paths = u_paths[3::]
v_paths = v_paths[3::]
sw_paths = sw_paths[3::]
q_paths = q_paths[3::]

def run_per_time(months, t_paths, u_paths, v_paths, sw_paths, q_paths, pot_pow_paths, pow_paths):

    for i, t_path in enumerate(t_paths):
        
        temp_tas = xr.open_dataset(t_path).tas
        temp_uas = xr.open_dataset(u_paths[i]).uas
        temp_vas = xr.open_dataset(v_paths[i]).vas
        temp_sw = xr.open_dataset(sw_paths[i]).rsds
        temp_q = xr.open_dataset(q_paths[i]).huss

        temp_pot_pow = xr.open_dataarray(pot_pow_paths[i])
        temp_pow = xr.open_dataarray(pow_paths[i])

        time = temp_tas.Time
        tind = []
        for j, t in enumerate(time):
            if pandas.Timestamp(t.values).month in months:
                tind.append(j)

        if i == 0:
            nav_lat = temp_tas.nav_lat_grid_M
            nav_lon = temp_tas.nav_lon_grid_M

            tas = temp_tas[tind].values
            uas = temp_uas[tind].values
            vas = temp_vas[tind].values
            sw = temp_sw[tind].values
            q = temp_q[tind].values
            pot_pow = temp_pot_pow[tind].values
            pows = temp_pow[tind].values

        else:
            tas = np.append(tas, temp_tas[tind].values, axis=0)
            uas = np.append(uas, temp_uas[tind].values, axis=0)
            vas = np.append(vas, temp_vas[tind].values, axis=0)
            sw = np.append(sw, temp_sw[tind].values, axis=0)
            q = np.append(q, temp_q[tind].values, axis=0)
            pot_pow = np.append(pot_pow, temp_pot_pow[tind].values, axis=0)
            pows = np.append(pows, temp_pow[tind].values, axis=0)

    tas_ave = np.nanmean(tas, axis=0)
    uas_ave = np.nanmean(uas, axis=0)
    vas_ave = np.nanmean(vas, axis=0)
    sw_ave = np.nanmean(sw, axis=0)
    q_ave = np.nanmean(q, axis=0)

    pot_pow_ave = np.nanmean(pot_pow, axis=0)
    pow_ave = np.nanmean(pows, axis=0)

    tas_std = np.nanstd(tas, axis=0)
    uas_std = np.nanstd(uas, axis=0)
    vas_std = np.nanstd(vas, axis=0)
    sw_std = np.nanstd(sw, axis=0)
    q_std = np.nanstd(q, axis=0)

    pot_pow_std = np.nanstd(pot_pow, axis=0)
    pow_std = np.nanstd(pows, axis=0)

    return tas_ave, tas_std, uas_ave, uas_std, vas_ave, vas_std, sw_ave, sw_std, q_ave, q_std, pot_pow_ave, pot_pow_std, pow_ave, pow_std, nav_lat, nav_lon

tas_ave, tas_std = [], []
uas_ave, uas_std = [], []
vas_ave, vas_std = [], []
sw_ave, sw_std = [], []
q_ave, q_std = [], []
pot_pow_ave, pot_pow_std = [], []
pow_ave, pow_std = [], []

# DJF, MAM, JJA, SON
monthss = [[12, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]

for months in monthss:

    temp_tas_ave, temp_tas_std, temp_uas_ave, temp_uas_std, temp_vas_ave, temp_vas_std, temp_sw_ave, temp_sw_std, temp_q_ave, temp_q_std, temp_pot_pow_ave, temp_pot_pow_std, temp_pow_ave, temp_pow_std, nav_lat, nav_lon = run_per_time(months, t_paths, u_paths, v_paths, sw_paths, q_paths, pot_pow_paths, pow_paths)

    tas_ave.append(temp_tas_ave)
    tas_std.append(temp_tas_std)

    uas_ave.append(temp_uas_ave)
    uas_std.append(temp_uas_std)

    vas_ave.append(temp_vas_ave)
    vas_std.append(temp_vas_std)

    sw_ave.append(temp_sw_ave)
    sw_std.append(temp_sw_std)

    q_ave.append(temp_q_ave)
    q_std.append(temp_q_std)

    pot_pow_ave.append(temp_pot_pow_ave)
    pot_pow_std.append(temp_pot_pow_std)

    pow_ave.append(temp_pow_ave)
    pow_std.append(temp_pow_std)

tas_ave = np.array(tas_ave)
tas_std = np.array(tas_std)

uas_ave = np.array(uas_ave)
uas_std = np.array(uas_std)

vas_ave = np.array(vas_ave)
vas_std = np.array(vas_std)

sw_ave = np.array(sw_ave)
sw_std = np.array(sw_std)

q_ave = np.array(q_ave)
q_std = np.array(q_std)

pot_pow_ave = np.array(pot_pow_ave)
pot_pow_std = np.array(pot_pow_std)

pow_ave = np.array(pow_ave)
pow_std = np.array(pow_std)

##############################################################################
# ave

tas_xr = xr.DataArray(tas_ave, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'], attrs=dict(description="seasonal averaged data from 1993 to 2013"))

uas_xr = xr.DataArray(uas_ave, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'], attrs=dict(description="seasonal averaged data from 1993 to 2013"))

vas_xr = xr.DataArray(vas_ave, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'], attrs=dict(description="seasonal averaged data from 1993 to 2013"))

sw_xr = xr.DataArray(sw_ave, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'], attrs=dict(description="seasonal averaged data from 1993 to 2013"))

q_xr = xr.DataArray(q_ave, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'], attrs=dict(description="seasonal averaged data from 1993 to 2013"))

pot_pow_xr = xr.DataArray(pot_pow_ave, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'], attrs=dict(description="seasonal averaged data from 1993 to 2013"))

pow_xr = xr.DataArray(pow_ave, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'], attrs=dict(description="seasonal averaged data from 1993 to 2013"))

# std

tas_std_xr = xr.DataArray(tas_std, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'], attrs=dict(description="standard deviation for the seasonal averaged data from 1993 to 2013"))

uas_std_xr = xr.DataArray(uas_std, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'], attrs=dict(description="standard deviation for the seasonal averaged data from 1993 to 2013"))

vas_std_xr = xr.DataArray(vas_std, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'], attrs=dict(description="standard deviation for the seasonal averaged data from 1993 to 2013"))

sw_std_xr = xr.DataArray(sw_std, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'], attrs=dict(description="standard deviation for the seasonal averaged data from 1993 to 2013"))

q_std_xr = xr.DataArray(q_std, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'], attrs=dict(description="standard deviation for the seasonal averaged data from 1993 to 2013"))

pot_pow_std_xr = xr.DataArray(pot_pow_std, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'], attrs=dict(description="standard deviation for the seasonal averaged data from 1993 to 2013"))

pow_std_xr = xr.DataArray(pow_std, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'], attrs=dict(description="standard deviation for the seasonal averaged data from 1993 to 2013"))

##############################################################################
# saving

save_path = root_path + 'Seasonal/'

#delayed = tas_xr.to_netcdf(save_path + 'seasonal_average_TAS.nc', compute=False)
#
#print('Saving TAS')
#with ProgressBar():
#    results = delayed.compute()
#
#delayed = uas_xr.to_netcdf(save_path + 'seasonal_average_UAS.nc', compute=False)
#
#print('Saving UAS')
#with ProgressBar():
#    results = delayed.compute()
#
#delayed = vas_xr.to_netcdf(save_path + 'seasonal_average_VAS.nc', compute=False)
#
#print('Saving VAS')
#with ProgressBar():
#    results = delayed.compute()
#
#delayed = sw_xr.to_netcdf(save_path + 'seasonal_average_RSDS.nc', compute=False)
#
#print('Saving RSDS')
#with ProgressBar():
#    results = delayed.compute()

delayed = q_xr.to_netcdf(save_path + 'seasonal_average_HUSS.nc', compute=False)

print('Saving HUSS')
with ProgressBar():
    results = delayed.compute()

#delayed = pot_pow_xr.to_netcdf(save_path + 'seasonal_average_EFF_IRR.nc', compute=False)
#
#print('Saving EFF IRR')
#with ProgressBar():
#    results = delayed.compute()
#
#delayed = pow_xr.to_netcdf(save_path + 'seasonal_average_POW.nc', compute=False)
#
#print('Saving POW')
#with ProgressBar():
#    results = delayed.compute()

delayed = tas_std_xr.to_netcdf(save_path + 'seasonal_std_TAS.nc', compute=False)

print('Saving TAS')
with ProgressBar():
    results = delayed.compute()

delayed = uas_std_xr.to_netcdf(save_path + 'seasonal_std_UAS.nc', compute=False)

print('Saving UAS')
with ProgressBar():
    results = delayed.compute()

delayed = vas_std_xr.to_netcdf(save_path + 'seasonal_std_VAS.nc', compute=False)

print('Saving VAS')
with ProgressBar():
    results = delayed.compute()

delayed = sw_std_xr.to_netcdf(save_path + 'seasonal_std_RSDS.nc', compute=False)

print('Saving RSDS')
with ProgressBar():
    results = delayed.compute()

delayed = q_std_xr.to_netcdf(save_path + 'seasonal_std_HUSS.nc', compute=False)

print('Saving HUSS')
with ProgressBar():
    results = delayed.compute()

#delayed = pot_pow_std_xr.to_netcdf(save_path + 'seasonal_std_EFF_IRR.nc', compute=False)
#
#print('Saving EFF IRR')
#with ProgressBar():
#    results = delayed.compute()
#
#delayed = pow_std_xr.to_netcdf(save_path + 'seasonal_std_POW.nc', compute=False)
#
#print('Saving POW')
#with ProgressBar():
#    results = delayed.compute()
