from glob import glob
import xarray as xr
import numpy as np

data_path = '/bdd/MEDI/workspaces/dkeller/SEAFUEL/NEMO/SolarIslands/HF'

paths = glob(data_path + '/*2D.nc')
paths.sort()

mlh = []
for i, path in enumerate(paths):
    print(i)
    data = xr.open_dataset(path)
    mlh.append(np.nanmean(data.somixhgt))

print(mlh)
print(np.nanmean(mlh))
