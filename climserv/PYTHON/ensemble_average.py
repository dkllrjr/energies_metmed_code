import xarray as xr
from glob import glob
import numpy as np
from dask.diagnostics import ProgressBar

##############################################################################

root_path = '/bdd/MEDI/workspaces/dkeller/SEAFUEL/NEMO/'

data_paths = glob(root_path + 'SolarIslands/DA/*2D.nc')
data_paths.sort()

##############################################################################
# averaging

data = []

sst = []
sal = []

for i, data_path in enumerate(data_paths):
    data.append(xr.open_dataset(data_path))
    
    temp_sst = data[-1].sosstsst
    temp_sst = temp_sst.drop(['time_counter', 'time_centered'])
    temp_sal = data[-1].sosaline
    temp_sal = temp_sal.drop(['time_counter', 'time_centered'])

    if data[-1].sosstsst.shape[0] < 366:

        if i == 0:
            nav_lat = temp_sst.nav_lat
            nav_lon = temp_sst.nav_lon

        tsst = xr.full_like(temp_sst[0], np.nan)
        sst.append(xr.concat([temp_sst[0:243], tsst, temp_sst[243::]], dim='time_counter').values)
        
        tsal = xr.full_like(temp_sal[0], np.nan)
        sal.append(xr.concat([temp_sal[0:243], tsal, temp_sal[243::]], dim='time_counter').values)

    else:
        sst.append(temp_sst.values)
        sal.append(temp_sal.values)

sst = np.array(sst)
sal = np.array(sal)

sst_ave = np.nanmean(sst, axis=0)
sal_ave = np.nanmean(sal, axis=0)

sst_xr = xr.DataArray(sst_ave, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'])
sal_xr = xr.DataArray(sal_ave, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'])

sst_std = np.nanstd(sst, axis=0)
sal_std = np.nanstd(sal, axis=0)

sst_std_xr = xr.DataArray(sst_std, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'], attrs=dict(description="standard deviation of the ensemble averaged from 1993 to 2013"))
sal_std_xr = xr.DataArray(sal_std, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'], attrs=dict(description="standard deviation of the ensemble averaged from 1993 to 2013"))

##############################################################################
# saving

save_path = root_path + 'Ensemble/'

# sst

filename = save_path + 'ensemble_average_SST_1D_2D.nc'
if os.path.exists(filename):
    print(filename, 'skipped')
else:
    delayed = sst_xr.to_netcdf(filename, compute=False)

    print('Saving SST')
    with ProgressBar():
        results = delayed.compute()

# salinity

filename = save_path + 'ensemble_average_SAL_1D_2D.nc'
if os.path.exists(filename):
    print(filename, 'skipped')
else:
    delayed = sal_xr.to_netcdf(filename, compute=False)

    print('Saving SAL')
    with ProgressBar():
        results = delayed.compute()

# sst std

filename = save_path + 'ensemble_std_SST_1D_2D.nc'
if os.path.exists(filename):
    print(filename, 'skipped')
else:
    delayed = sst_std_xr.to_netcdf(filename, compute=False)

    print('Saving SST')
    with ProgressBar():
        results = delayed.compute()

# salinity std

filename = save_path + 'ensemble_std_SAL_1D_2D.nc'
if os.path.exists(filename):
    print(filename, 'skipped')
else:
    delayed = sal_std_xr.to_netcdf(filename, compute=False)

    print('Saving SAL')
    with ProgressBar():
        results = delayed.compute()
