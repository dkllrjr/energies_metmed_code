import xarray as xr
from glob import glob
import numpy as np
from dask.diagnostics import ProgressBar

##############################################################################

root_path = '/gpfsstore/rech/ron/uhj52bh/SEAFUEL/WRF/'

huss_paths = glob(root_path + 'WRF_NemoBulk/*HUSS.nc')
huss_paths.sort()

psl_paths = glob(root_path + 'WRF_NemoBulk/*PSL.nc')
psl_paths.sort()

##############################################################################
# averaging

huss_paths = huss_paths[-21::]
psl_paths = psl_paths[-19::]

huss = []
psl = []

for i, huss_path in enumerate(huss_paths):
    
    temp_huss = xr.open_dataset(huss_path).huss

    if i == 0:
        nav_lat = temp_huss.nav_lat_grid_M
        nav_lon = temp_huss.nav_lon_grid_M

    if temp_huss.shape[0] < 2928:
        thuss = xr.full_like(temp_huss[0:8], np.nan)
        huss.append(xr.concat([temp_huss[0:480], thuss, temp_huss[480::]], dim='Time').values)

        print('yes')

    else:
        huss.append(temp_huss.values)

# psl

for i, psl_path in enumerate(psl_paths):
    
    temp_psl = xr.open_dataset(psl_path).psl

    if temp_psl.shape[0] < 2928:
        tpsl = xr.full_like(temp_psl[0:8], np.nan)
        psl.append(xr.concat([temp_psl[0:480], tpsl, temp_psl[480::]], dim='Time').values)

        print('yes')

    else:
        psl.append(temp_psl.values)


huss = np.array(huss)
psl = np.array(psl)

huss_ave = np.nanmean(huss, axis=0)
psl_ave = np.nanmean(psl, axis=0)

huss_xr = xr.DataArray(huss_ave, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'], attrs=dict(description="ensemble averaged data from 1993 to 2013"))
psl_xr = xr.DataArray(psl_ave, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'], attrs=dict(description="ensemble averaged data from 1993 to 2013"))

huss_std = np.nanstd(huss, axis=0)
psl_std = np.nanstd(psl, axis=0)

huss_std_xr = xr.DataArray(huss_std, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'], attrs=dict(description="standard deviation for the ensemble averaged data from 1993 to 2013"))
psl_std_xr = xr.DataArray(psl_std, coords=dict(lon=(['y', 'x'], nav_lon), lat=(['y', 'x'], nav_lat)), dims=['t', 'y', 'x'], attrs=dict(description="standard deviation for the ensemble averaged data from 1993 to 2013"))

##############################################################################
# saving

save_path = root_path + 'ENSEMBLE/'

delayed = huss_xr.to_netcdf(save_path + 'ensemble_average_HUSS.nc', compute=False)

print('Saving HUSS')
with ProgressBar():
    results = delayed.compute()

delayed = psl_xr.to_netcdf(save_path + 'ensemble_average_PSL.nc', compute=False)

print('Saving PSL')
with ProgressBar():
    results = delayed.compute()

# 

delayed = huss_std_xr.to_netcdf(save_path + 'ensemble_std_HUSS.nc', compute=False)

print('Saving HUSS')
with ProgressBar():
    results = delayed.compute()

delayed = psl_std_xr.to_netcdf(save_path + 'ensemble_std_PSL.nc', compute=False)

print('Saving PSL')
with ProgressBar():
    results = delayed.compute()
